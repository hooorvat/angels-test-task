# Общие положения:

При разработке используется подход graceful degradation. Верстка должна корректно отображаться во всех современных браузерах.
Верстка должна соответствовать макетам на 98%. Расхождение с макетами допускается только в случае явной ошибки дизайнера или невозможности сделать так же.
Для сверки верстки использовать расширение браузера Perfect Pixel.

## Структура:

Файлы стилей, изображения, шрифты и js находятся в папке src. Необходимо соблюдать следующую структуру:

* **/src**
	* /fonts
	* /images
	* /js
	* /less
	* /vendor

## Изображения:

Все иконки должны быть упакованы в спрайт с именем sprite.png.
Для ретина дисплеев файл должен иметь суффикс retina `sprite-retina.png`.

Для картинок фонов использовать суффикс bg `header-bg.png`.

Имена файлов должны быть понятны. Написаны строчными буквами. При многосоставных названиях использовать дефис. Использование Camel Case нотации недопустимо.
```
logo, header-bg - правильно
blue, 1, hb - не правильно
```

Использование нижнего подчеркивания только для отображения состояния:
```
add-button_default.png
add-button_hover.png
add-button_active.png
```

Изображения должны быть в папке /src/images. Если отдельная сущность предполагает большое количество изображений, то допустимо создать для них папку.
```
/src/images/users
```

## Html

###### Общее:
Необходимо использовать корпоративный код-стиль.
Использование id допускается только в случаях явной необходимости и невозможности использовать class.
Каждый тег начинается с новой строки. Для отступов используются табы.

###### Наименование и структура:
При наименовании блоков необходимо руководствоваться следующими правилами:

-	Вся верстка поделена на независимые блоки - сущности.
-	Сущности, относящиеся к общей разметке (layout) имеют префикс **p-** `p-header, p-footer`.
-	Сущности которые присутствуют во множестве других сущностей (единое оформление кнопок, подсказок и т.д.) имеют префикс **g-** `g-button, g-icon`.
-	Независимые сущности имеют префикс **b-** (блок новостей, карточка пользователя и т.д.) `b-news, b-user, b-social-likes`.
-	Формы и элементы в них имеют префикс **f-** `f-container, f-element-text`.
-	Названия должны отображать суть сущности `b-news, p-header` - правильно, `red-block, greenLine` - не правильно
-	Внутри сущностей использовать простые наименования `title, description, count, user-rating`.
-	В многосоставных названиях использовать дефис. Использование заглавных букв и нижнего подчеркивания недопустимо.
-	Для списков элементов использовать классы .items > .item

## Стили

##### Файлы:

* Миксины `mixins.less`
* Переменные `variables.less`
* Обнуление стилей `reset.less`
* Подключение шрифтов `fonts.less`
* Оформления глобальных элементов `global.less`
* Оформления общих для всех страниц элементов, layout `page.less`
* Оформления блоков `blocks.less`
* Оформление форм `forms.less`

В больших проектах допускается создавать файлы соответствующие названиям сущностей, которые должны находится в папке /src/less/blocks.

* **/less**
	* /blocks
        * news.less
        * user.less
	* mixins.less
	* variables.less
	* reset.less
	* fonts.less
	* global.less
	* page.less
	* blocks.less
	* forms.less

###### Наименования классов:
Css классы записываются строчными буквами. Заглавные буквы не допускаются. При многосоставном названии используется дефис.
```
b-news, g-button - правильно
bigRedButton, news_list - не правильно
```

###### Правила написания:
* Все правила должны применяться к классам. Применение блоков правил к тегам допускается только в случае явной необходимости.
* Необходимо знать и использовать стандартный набор миксинов используемых во всех проектах.
* При написании правил необходимо руководствоваться код-стилем компании. Файл codestyle.less это касается как формы записи, расстановки пробелов так и порядка свойств.
* Вложенность элементов в less не должна строго соответсвовать html структуре, в таком случае потеряется гибкость при поддержке. 
Желательно стремиться к минимальной вложенности, выделяя при этом сущности в отдельные блоки.

```less
.b-block {
	.wrapper {...}
	.inner {...}
	
	.items {...}
	
	.item {
		.title {}
		.description {}
		.image {}
	}
}
```

## Шрифты

Шрифты необходимо конвертировать для кроссбраузерного отображения.

Сервисы для конвертирования:
[webfont-generator](http://www.fontsquirrel.com/tools/webfont-generator)
[flowyapps](http://fontie.flowyapps.com/)

Важно не забыть указать при конвертации необходимость кириллицы.

## JavaScript

[documentation/javascript.md](documentation/javascript.md)

## Git

* Названия веткам даются по шаблону **issue_###_description** без прописных букв.

```
"issue_1867_imageMap_improvement" - не правильно
"issue_2951_wedding_service" - правильно
```

* В названии веток допускаются только английские слова, транслитерация запрещена.
* Описание коммитов на русском языке, в безличной форме, в прошедшем времени `Запилена фича ...`, `Решена проблема ...`. 
То есть описание коммита отвечает на вопрос *какая была проведена работа?*, либо это описание содержимого коммита `сборка`, `правки по код стилю`.
* В описании коммита нужно указывать на контекст или область, к которой относится коммит.
 
```
"Исправлена кнопка" - не правильно
"Исправлена кнопка в шапке главной страницы" - правильно
```

## Чеклист для контроля на инициализацию проекта

- стуктура проекта выбрана под бэкенд (битрикс/cms/фреймворк)
- сборщик настроен на тестовые сборки
- подготовлена индексная страница [indexExample.php](indexExample.php)
- сетка правильно подобрана и покрывает все нужды проекта
- подготовлена вставка с пшп префиксом для ресурсов [templates/header.php](templates/header.php)
- проверены глобальные стили на всех макетах
- в гите мастерами добавлены бэкенд разработчики

## Чеклист для контроля на сдачу задачи

- проверить наличие ховеров на активных элементах
- адаптив по tap tap, если к адаптиву нет других требований
- внешние ссылки с target="_blank"
- в шаблонах все пути относительные (url-ы аяксов, картинок, экшенов форм)
- картинки в шаблонах для битрикса с пшп префиксом
- в жс картинки завёрнуты в require('относительный путь')
- alt на картинках
- проверить глобальные стили на других макетах
- новая страница добавлена в индекс
- тестовая страница опубликована в чате фронтенд отдела или продемонстрирована ПМу/дизайнеру
- тестовая страница залита на дев-сервер, если дев-сервер используется для демонстрации
- вёрстка подготовлена для бэкенда - залита сборка, оставлены комментарии при сложных случаях в вёрстке, мердж реквест описан в общем чате проекта
- отписаться в комментарии что сделано по задаче на своё усмотрение и как
- если задача реализована частично - оставить комментарии с пояснением причин
- проверить не ломается ли аналитика при изменениях в макетах
- все скрипты с внешних сервисов должны подключаться с автовыбором протокола (через //)