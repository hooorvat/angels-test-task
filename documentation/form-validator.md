FormValidator
===================

Создан для валидации форм. 

> **Основные отличия от jQuery Validation Plugin: **

> - отправка формы аяксом по умолчанию
> - обработка ответа сервера в соответствии с форматом обмена данными Doctornet.pro

----------

Использование
-------------

```
$('.js-form').each(function () {
		new FormValidator($(this), {
			rules: {
				file: {
					filesize: 5 * 1024 * 1024
				},
				phone: {
					regexp: /^([\+]+)*[0-9\x20\x28\x29\-]{5,20}$/
				}
			},
			messages: {
				file: {
					filesize: 'Очень большой размер файла' // Кастомное сообщение для правила конкретного поля.
				},
			}
		});
	});
```


Опции
-------------

```
var defaultOptions = {
	ajax: {  // Если null будет работать только валидация, без отправки аяксом
		dataType: 'json'
	},
	sendType: 'serialize', // serialize, formData, json
	classes: { // Классы отображающие соостояние
		error: 'error',
		valid: 'valid'
	},
	clean: true, // Очистка формы при удачной отправке
	redirect: false, // Если в ответе пришло поле redirect произойдет переадресация
	rules: {}, // Плавила для полей
	messages: {}, // Сообщения для полей
	fieldSelector: '.f-field', // Селектор родительского контейнера
	onError: null, // Коллбек ошибки
	onSuccess: null // Коллбек успешной отправки
};
```

#### Немного подробнее об опциях

- ajax: либо null либо представляет собой объект который обычно передается в $.ajax()
- sendType:  Метод сериализации формы 
	-  serialize  - form.serialize(),
	-  formData - new FormData(this.nodes.form.get(0))
	-  json - JSON.stringify({ "inputName" : "inputValue", ... })
- Коллбеки - позволяют обрабатывать события формы. Если на одну форму будет создано несколько  new FormValidator($(this)); То повторно инициализация не произойдет и будет использоваться первый экземпляр. Но коллбеки заменятся на коллбеки последнего вызова. Стоит отметить что изначально подразумевалось что опции и коллбеки будут складываться с каждой инициализацией, но это оказалось не нужным. 


```
<div class="f-form b-callback-form js-form js-callback-form">
	<div class="g-head-line-main-text">Заказать обратный звонок</div>
	<div class="f-base-message"></div>
	<form action="/ajax/formHandler.php">
		<div class="f-field required">
			<label class="f-label" for="name">Имя</label>
			<span class="f-message"></span>
			<input name="name" class="f-input" type="text" required>
		</div>
		<div class="f-field required">
			<label class="f-label" for="name">Телефон</label>
			<span class="f-message"></span>
			<input name="phone" class="f-input js-phone-input" type="tel" required>
			<div class="js-reset-input" data-name="file">Сбросить поле</div>
		</div>
		<input class="f-submit g-primary-action-button" type="submit" value="Отправить заявку">
	</form>
</div>
```

> **Особенности**

> - f-base-message - Сообщения об общих ошибках валидации, например ошибка при отправке аякса (404) или сообщение с бека без привязки к  полю.
> - f-message - Сообщения для конкретного поля, используется как для вывода ошибок с фронта, так и с бэка.
> - js-reset-input при клике по элементу произойдет пересоздания поля с именем записанном в data-name="file", полезно при очистке поля типа файл.
> - Использование атрибутов поля.
>	 - type - если значение атрибута равно email, tel и url будет применено соответствующее правило (регулярное выражение стандартное для данного типа поля)
>	 - pattern - Если у поля есть этот атрибут к нему будет применено правило regexp со знанием атрибута.
>	 - required - использование этого атрибута указывает на обязательность поля.
> - Использование классов поля
>	 - Поля имеющие классы email, tel или url будут валидироваться также как с соответствующим типом.

**Важно уточнить что правила записанные атрибутами в html из опций имеют больший приоритет чем правила инициализации в JS**

#### Стандартные правила

> - required :  boolean
> - regexp : RegExp
> - email :  boolean
> - phone :  boolean
> - url :  boolean
> - filesize: number
> - extension: string

#### Добавление кастомных правил
```
$$.validator.addMethod('filesize', function (value, element, size) {
	return !element.get(0).files[0] || element.get(0).files[0].size <= size;
}, 'Попробуйте загрузить файл поменьше');
```
Метод \$$.validator.addMethod(rule, func, message) принимает три аргумента

- rule - название правила
- func - функцию обработчик
- message - сообщение об ошибке

Функция обработчик вызывается при изменении значения поля или отправке формы. 
В нее передается:

- Значение поля
- jquery элемент
- парметр который задавался при инициализации валидатора, в данном примере размер файла (5 * 1024 * 1024)

```
{
	rules: {
		file: {
			filesize: 5 * 1024 * 1024
		},
}
```
