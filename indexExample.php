<?php

$pageUrl = preg_replace('/\\?.*/', '', trim($_SERVER[REQUEST_URI], '/'));

include('./templates/header.php');
include('./templates/pages/'. ( $pageUrl ? $pageUrl : 'index' ) . '.php');
include('./templates/footer.php');
