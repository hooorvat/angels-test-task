<?php

namespace Site\Bootstraps {

	use \Yii;

	class Main extends \Cms\SiteBootstrap
	{
		public function run()
		{
			if (\Config::$debug && !is_dir(ROOT_PATH . DIRECTORY_SEPARATOR . 'site/bundle')) {
				Yii::app()->viewRenderer->getTwig()->addGlobal('webpackDevUrl', '//localhost:8080');
			}
		}
	}
}
