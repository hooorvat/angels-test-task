window.$$ = window.$$ || {};

import '../less/reset.less';
import '../less/fonts.less';
import '../less/icons.less';
import '../less/global.less';
import '../less/page.less';
import '../less/blocks.less';

import WallManager from './Components/WallManager.es6';

class Application {
    constructor() {
        this._initWallManager();
    }

    _initWallManager() {
        // Если элемента не существует, то Each не отработает
        $('.js-wall').each(function () {
            $$.wall = new WallManager(this, {
                step: 20, // количество записей, подгружаемых за раз

                // VkApi
                api: {
                    domain: 'extrawebdev',
                    token: '3195e9e93195e9e93115aff35931c30219331953195e9e9697cc67864e1f0c35fe7c074'
                }
            });
        });
    }
}

$(function () {
    $$.window = $(window);
    $$.body = $('body');
    $$.document = $(document);

    $$.windowWidth = $$.window.width();
    $$.windowHeight = $$.window.height();

    $$.window.on('resize', () => {
        $$.windowWidth = $$.window.width();
        $$.windowHeight = $$.window.height();
    });

    $$.application = new Application();
});
