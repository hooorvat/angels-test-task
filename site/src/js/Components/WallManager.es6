import * as _ from 'lodash';

export default class WallManager {
    constructor(root, options) {
        // Стандартные настройки
        const defaultOptions = {
            step: 20,

            api: {
                version: 5.60
            }
        };

        this.root = $(root); // Корневой элемент
        this.options = _.assign(defaultOptions, options); // Склеиваем
                                                          // дефолтные
                                                          // параметры и те,
                                                          // что мы указали
        this.currentPage = 0; // Изначально берем с 0 поста
        this.lockAutoloadPosts = false; // Блокировка автоподгрузки

        this._cacheNodes(); // Кэшируем DOM-компоненты
        this._bindEvents(); // Подписываем события
        this._initialize(); // Инициализируемся
    }

    _cacheNodes() {
        this.nodes = {
            wall: this.root // На данный момент у нас только один элемент
                            // Сделал для того, чтобы можно было расширить в
                            // будущем (конечно же нет).
        };
    }

    _bindEvents() {
        $(window).on('scroll', () => {
            // Добавляем 150 пикселей, чтобы срабатывало немного раньше, чем мы
            // докрутим до конца документа
            if ($$.window.scrollTop() + $$.window.height() + 150 >= $(document).height()) {
                if (!this.lockAutoloadPosts) { // Если нет блокировки
                    this.lockAutoloadPosts = true; // Ставим ее

                    this._loadPosts(this.currentPage * this.options.step, this.options.step)
                        .then(() => {
                            this.lockAutoloadPosts = false; // При прогрузке и
                                                            // отображении
                                                            // постов, снимаем
                                                            // блокировку
                        });
                }
            }
        });
    }

    _initialize() {
        this._loadPosts(); // Загружаем первую пачку постов
    }

    _loadPosts(start = this.currentPage, limit = this.options.step) {
        return new Promise(resolve => {
            // Сначала поллучаем массив с постами
            this._getPostsWithApi(start, limit)
                .then(posts => {
                    ++this.currentPage;

                    // А затем преобразовываем их и отображаем
                    _.forEach(posts, post => {
                        const wrapper = this._createPostComponent(post);

                        if (wrapper !== null) {
                            this.nodes.wall.append(wrapper);
                            wrapper.css('display', 'none');

                            wrapper.fadeIn(500);
                        }
                    });

                    // После чего вызываем колбек
                    resolve();
                });
        });
    }

    _getPostsWithApi(start = this.currentPage, limit = this.options.step) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: 'https://api.vk.com/method/wall.get',
                data: {
                    domain: this.options.api.domain,
                    access_token: this.options.api.token,

                    offset: start,
                    count: limit,

                    version: this.options.api.version,
                    extended: 1
                },

                method: 'get',
                jsonp: 'callback',
                dataType: 'jsonp', // Нужно для кроссдоменных запросов

                success: data => {
                    if (data.response.wall) {
                        resolve(data.response.wall); // Если есть массив с
                                                     // постами, то все хорошо
                    } else {
                        reject(data); // Иначе ошибка
                    }
                },

                error: error => {
                    reject(error); // Ошибочка при ajax
                }
            });
        });
    }

    _createPostComponent(post) {
        // Первый аргумент в массиве - это число. Поэтому игнорируем это
        if (typeof post === 'number') {
            return null;
        }

        // Создаем контейнеры
        const wrapper = $('<a target="_blank" class="b-post js-post" />');
        const attachmentWrapper = $('<div class="b-attachment" />');
        const desctiptionWrapper = $('<div class="description" />');

        // Текст для отображения
        let postText = null;

        // Вложения
        const attachments = post.attachments && post.attachments.length > 0 ? post.attachments : [];

        // Если они есть
        if (attachments.length > 0) {
            // Нам нужно первое вложение
            const attachment = attachments[0];
            wrapper.attr('data-type', attachment.type); // Делал для отладки
                                                        // (так обнаружил, что
                                                        // есть посты с
                                                        // видосами)

            // По ТЗ обрабатываю только фото и ссылку
            switch (attachment.type) {
                case 'photo' : {
                    const content = attachment.photo;

                    // Картинка, с самым большим расширением
                    attachmentWrapper.append(`<img class="attachment" src="${content.src_xxbig}">`);
                    postText = content.text; // Подпись к картинке, она же и
                                             // равна самому заголовку

                    wrapper.append(attachmentWrapper); // Добавляем в контейнер

                    break;
                }

                case 'link' : {
                    // В принципе все аналогично, как и выше.
                    const content = attachment.link;

                    postText = content.description;
                    wrapper.attr('href', content.url);

                    if (content.image_big) {
                        attachmentWrapper.append(`<img class="attachment" src="${content.image_big}">`);
                        wrapper.append(attachmentWrapper);
                    }

                    break;
                }

                default : {
                    // Берем просто описание (например, если во вложении видео)
                    postText = post.text;

                    break;
                }
            }
        } else {
            // Если вложений нет, то берем просто текст

            postText = post.text;
        }

        // Парсим ссылки и хештеги (сделал парсинг хеш тегов только для вида
        // #word@group)
        postText = this._generateLinks(postText);
        desctiptionWrapper.html(postText);

        if (postText !== null && postText.length > 0) {
            wrapper.append(desctiptionWrapper);
        }

        // Генерируем прямую ссылку на пост, если возможно
        if (post.copy_owner_id && post.copy_post_id) {
            const link = `https://vk.com/wall${post.copy_owner_id}_${post.copy_post_id}`;
            wrapper.attr('href', link);
        }

        // Если нет вложений
        if (wrapper.find('.attachment').length === 0) {
            let src = '';

            // Берем адрес всего контейнера, если возможно
            if (wrapper.attr('href') && wrapper.attr('href').length > 0) {
                src = wrapper.attr('href');
            }

            // Ссылки внутри - приоритетнее, берем самую первую, если есть
            const links = wrapper.find('a');
            if (links.length > 0) {
                src = links.eq(0).attr('href');
            }

            // Как фича: воспользуемся небольшим сервисом для генерации
            // скриншотов сайтов, и сделаем подобия вложений
            if (src && src.length > 0) {
                attachmentWrapper.append(`<img class="attachment" src="http://mini.s-shot.ru/1024x768/600/jpeg/?${src}">`);
                wrapper.append(attachmentWrapper);
            }
        }

        return wrapper;
    }

    _generateLinks(text) {
        // Если текста нет
        if (text === null) {
            return null;
        }

        // А иначе погнали преобразовывать
        let result = text;

        // Генерируем ссылки
        result = result.replace(/(https?:\/\/[\\.a-zA-Z0-9-_#?\/=&%]+)/, match => {
            return `<a href="${match}" target="_blank" class="post-link">${match}</a>`;
        });

        // Генерируем ссылки на хеш-теги
        result = result.replace(/#(\S+)/g, (match, p1) => {
            const p = p1.split('@');
            if (p.length > 1) {
                return `<a href="https://vk.com/${p[1]}/${p[0]}" target="_blank" class="post-link">#${p[0]}@${p[1]}</a>`;
            }

            return `<a href="https://vk.com/feed?q=%23${p1}&section=search" target="_blank" class="post-link">#${p[0]}</a>`;
        });

        return result;
    }
}
