<div class="b-menu-fixed-container js-menu-fixed">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="wrapper-menu">
                    <a href="MainPage?test" class="logo-menu-fixed js-open-menu-fixed-top">
                        <img src="<?= $php_prefix ?>./site/src/images/images-static/logo.png" alt="">
                    </a>
                    <a href="tel:84732622233" class="phone js-open-menu-fixed-top">
                        8 473 262-22-33
                    </a>
                    <span class="link js-open-menu-fixed-top">|</span>
                    <a href="#" class="link col-sm-hide js-open-menu-fixed-top js-popup-open" data-target="callbackPopup">
                        Перезвонить
                    </a>
                    <a href="" class="link enroll js-open-menu-fixed-top js-popup-open" data-target="enrollPopup">
                        Записаться
                    </a>
                    <div class="list-menu">
                        <div class="items-menu js-open-menu-fixed-top">
                            <a href="ServicesCommonPage?test" class="items-menu title-link active">
                                Услуги
                                <span class="g-icon icon-drop-down-arrow"></span>
                            </a>
                            <ul class="drop-down-container">
                                <li class="drop-down-item">
                                    <a href="#" class="drop-down-item link">
                                        Акции и скидки
                                    </a>
                                </li>
                                <li class="drop-down-item">
                                    <a href="KidsServicesCommonPage?test" class="drop-down-item link">
                                        Детское отделение
                                    </a>
                                </li>
                                <li class="drop-down-item">
                                    <a href="ServicesCommonPage?test#diagnostic" class="drop-down-item link">
                                        Диагностика
                                    </a>
                                </li>
                                <li class="drop-down-item">
                                    <a href="ServicesCommonPage?test#cataract-cure" class="drop-down-item link">
                                        Лечение катаракты
                                    </a>
                                </li>
                                <li class="drop-down-item">
                                    <a href="ServicesCommonPage?test#glaucoma-cure" class="drop-down-item link">
                                        Лечение глаукомы
                                    </a>
                                </li>
                                <li class="drop-down-item">
                                    <a href="ServicesCommonPage?test#retina-cure" class="drop-down-item link">
                                        Лазеркоагуляция сетчатки
                                    </a>
                                </li>
                                <li class="drop-down-item">
                                    <a href="ServicesCommonPage?test#restoring-cure" class="drop-down-item link">
                                        Восстановительное лечение глаз
                                    </a>
                                </li>
                                <li class="drop-down-item">
                                    <a href="ServicesCommonPage?test#other-services-and-operations" class="drop-down-item link">
                                        Прочие операции и услуги
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <a href="/KidsServicesCommonPage?test" class="items-menu js-open-menu-fixed-top">
                            Детское отделение
                        </a>
                        <div class="items-menu js-open-menu-fixed-top">
                            <a class="items-menu title-link" href="/AboutPage?test" >
                                О клинике
                                <span class="g-icon icon-drop-down-arrow"></span>
                                <span class="divider"></span>
                            </a>
                            <ul class="drop-down-container">
                                <li class="drop-down-item">
                                    <a class="drop-down-item link js-popup-open" data-target="enrollPopup">
                                        Запись на прием
                                    </a>
                                </li>
                                <li class="drop-down-item">
                                    <a href="GalleryPage?test" class="drop-down-item link">
                                        Галерея
                                    </a>
                                </li>
                                <li class="drop-down-item">
                                    <a href="NewsCommonPage?test" class="drop-down-item link">
                                        Новости и статьи
                                    </a>
                                </li>
                                <li class="drop-down-item">
                                    <a href="FAQPage?test" class="drop-down-item link">
                                        Вопросы и ответы
                                    </a>
                                </li>
                                <li class="drop-down-item">
                                    <a href="LegalInformationPage?test" class="drop-down-item link">
                                        Правовая информация
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <a href="/ContactsPage?test" class="items-menu js-open-menu-fixed-top">
                            Контакты
                        </a>
                        <a class="items-menu js-open-search js-open-menu-fixed-top">
                            Поиск
                            <span class="g-icon icon-search"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="search-panel-container js-search-panel">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-6 col-md-6 col-md-offset-6 col-lg-6 col-lg-offset-6">
                    <div class="search-wrapper">
                        <input class="search-input" type="text" placeholder="Запрос">
                        <span class="g-icon icon-search"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>