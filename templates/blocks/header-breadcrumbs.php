<div class="b-breadcrumbs">
    <ol class="items-container" itemscope itemtype="http://schema.org/BreadcrumbList">
        <li class="item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a class="text" href="/" title="Главная" itemprop="item">
				<span itemprop="name">
					Главная
				</span>
            </a>
            <i class="separation-slash">/</i>
            <meta itemprop="position" content="1"/>
        </li>
        <li class="item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a class="text" href="/ucatalog/" title="Услуги" itemprop="item">
				<span itemprop="name">
					Услуги
				</span>
            </a>
            <i class="separation-slash">/</i>
            <meta itemprop="position" content="2"/>
        </li>
        <li class="item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <a class="text" href="/ucatalog/" title="Лечение катаркаты" itemprop="item">
				<span itemprop="name">
					Лазеркоагуляция сетчатки
				</span>
            </a>
            <i class="separation-slash">/</i>
            <meta itemprop="position" content="2"/>
        </li>
    </ol>
</div>