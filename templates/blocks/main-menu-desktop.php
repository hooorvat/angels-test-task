<div class="b-navigation-menu-desktop">
    <div class="container">
        <div class="service-menu-wrapper">
            <div class="row">
                <div class="col-sm-3 col-md-3 col-lg-3">
                    <a href="MainPage?test" class="logo">
                        <div class="g-icon icon-large-logo"></div>
                    </a>
                </div>
                <div class="col-sm-3 col-md-2 col-lg-2">
                    <div class="info-item">
                        <div class="icon-kind-dept-container">
                            <span class="g-icon icon-kind-dept"></span>
                        </div>
                        <a href="KidsServicesCommonPage?test" class="link kids-link">
                            Детское отделение офтальмологии
                        </a>
                    </div>
                </div>
                <div class="col-sm-3 col-md-2 col-md-offset-1 col-lg-2 col-lg-offset-1">
                    <div class="info-item">
                        <div class="icon-callback-container">
                            <span class="g-icon icon-callback"></span>
                        </div>
                        <a href="tel:8473-262-22-33" class="telephone">
                            8 473 262-22-33
                        </a>
                        <a href="#" class="link js-popup-open" data-target="callbackPopup">
                            Перезвонить
                        </a>
                    </div>
                </div>
                <div class="col-sm-3 col-md-2 col-md-offset-1 col-lg-2 col-lg-offset-1">
                    <div class="info-item">
                        <div class="icon-consultation-container">
                            <span class="g-icon icon-consultation"></span>
                        </div>
                        <a href="" class="link js-popup-open" data-target="enrollPopup">
                            Записаться <br> на&#160;консультацию
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="b-menu">
            <div class="row">
                <div class="menu-desktop">
                    <div class="container-menu">
                        <div class="main-menu-title js-open-menu-first-level">
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <a href="ServicesCommonPage?test" class="main-menu-title">
                                    Услуги
                                    <span class="g-icon icon-drop-down-arrow"></span>
                                    <div class="divider-main-menu"></div>
                                </a>
                                <div class="container-menu-first-level js-menu-first-level">
                                    <ul class="list-menu-first-level">
                                        <li class="item-menu-first-level js-open-menu-second-level">
                                            <a href="#" class="item-menu-first-level link">
                                                Акции скидки
                                                <span class="g-icon icon-drop-down-arrow-first-level"></span>
                                            </a>
                                            <div class="divider-list-first-level"></div>
                                            <div class="container-menu-second-level js-menu-second-level">
                                                <ul class="list-menu-second-level">
                                                    <li class="item-menu-second-level js-hyphenated">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаркты 1 категории сложности без имплантации ИОЛ
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level js-hyphenated">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаркты 2 категории сложности без имплантации ИОЛ
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level js-hyphenated">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаркты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level js-hyphenated">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаркты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level js-hyphenated">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаркты 1 категории сложности с имплантацией монофокальной ИОЛ Alcon Acrysof IQ
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level js-hyphenated">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаркты 2 категории сложности с имплантацией монофокальной ИОЛ Alcon Acrysof IQ
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level js-hyphenated">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаркты 1 категории сложности с имплантацией монофокальной ИОЛ Abbott Tecnis
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level js-hyphenated">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаркты 2 категории сложности с имплантацией монофокальной ИОЛ Abbott Tecnis
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level js-hyphenated">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаркты (1 категории) с имплантацией торической ИОЛ Alcon Acrysof IQ Toric
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level js-hyphenated">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаркты (2 категории) с имплантацией торической ИОЛ Alcon Acrysof IQ Toric
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level js-hyphenated">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаркты с имплантацией мультифокальной ИОЛ Alcon Acrysof Restor
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level js-hyphenated">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаркты с имплантацией мультифокальной торической ИОЛ Alcon Acrysof Restor Toric
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level js-hyphenated">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаркты (1 категории) с имплантацией монофокальной ИОЛ HumanOptics Aspira-aAY
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level js-hyphenated">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаркты (2 категории) с имплантацией монофокальной ИОЛ HumanOptics Aspira-aA
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level js-hyphenated">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаркты (1 категории) с имплантацией монофокальной ИОЛ Zess CT ASPHINA 409M
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level js-hyphenated">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаркты (2 категории) с имплантацией монофокальной ИОЛ Zess CT ASPHINA 409M
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="item-menu-first-level">
                                            <a href="KidsServicesCommonPage?test" class="item-menu-first-level link">
                                                Детское отделение
                                            </a>
                                            <div class="divider-list-first-level"></div>
                                        </li>
                                        <li class="item-menu-first-level">
                                            <a href="ServicesCommonPage?test#diagnostic" class="item-menu-first-level link">
                                                Диагностика
                                            </a>
                                            <div class="divider-list-first-level"></div>
                                        </li>
                                        <li class="item-menu-first-level">
                                            <a href="ServicesCommonPage?test#cataract-cure" class="item-menu-first-level link">
                                                Лечение катаракты
                                            </a>
                                            <div class="divider-list-first-level"></div>
                                        </li>
                                        <li class="item-menu-first-level">
                                            <a href="ServicesCommonPage?test#glaucoma-cure" class="item-menu-first-level link">
                                                Лечение глаукомы
                                            </a>
                                            <div class="divider-list-first-level"></div>
                                        </li>
                                        <li class="item-menu-first-level">
                                            <a href="ServicesCommonPage?test#retina-cure" class="item-menu-first-level link">
                                                Лазеркоагуляция сетчатки
                                            </a>
                                            <div class="divider-list-first-level"></div>
                                        </li>
                                        <li class="item-menu-first-level js-open-menu-second-level">
                                            <a href="ServicesCommonPage?test#restoring-cure" class="item-menu-first-level link">
                                                Восстановительное лечение глаз
                                            </a>
                                            <span class="g-icon icon-drop-down-arrow-first-level text-two-lines"></span>
                                            <div class="divider-list-first-level"></div>
                                            <div class="container-menu-second-level js-menu-second-level">
                                                <ul class="list-menu-second-level">
                                                    <li class="item-menu-second-level">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификациз имплонтации ИОЛ
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаракты 1 категории сложности без имплонтации ИОЛ Abbott Tecnis Факоэмульсификация катаракты 1 категории сложности без имплонтации ИОЛ Abbott Tecnis
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаракты категории сложности с имплонтацией ИОЛ Acrysof Restor Toric Факоэмульсификация катаракты 1 категории сложности без имплонтации ИОЛ Abbott Tecnis Факоэмульсификация катаракты 1 категории сложности без имплонтации ИОЛ Abbott Tecnis
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификаци без имплонтации ИОЛ
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсости без имплонтации ИОЛ Abbott Tecnis
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмимплонтацией ИОЛ Acrysof Restor Toric
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсифлонтации ИОЛ
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаракты 1 категории сложности без имплонтации ИОЛ Abbott Tecnis Факоэмульсификация катаракты 1 категории сложности без имплонтации ИОЛ Abbott Tecnis
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаракты категФакоэмульсификация катаракты 1 категории сложности без имплонтации ИОЛ Abbott Tecnisории сложности с имплонтацией ИОЛ Acrysof Restor Toric
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаракты Факоэмульсификация катаракты 1 категории сложности без имплонтации ИОЛ Abbott Tecnis 2 категории сложности без имплонтации ИОЛ
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаракты 1 катии ИОЛ Abbott Tecnis
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаракты категории сложности с имплонтацией ИОЛ Acrysof Restor Toric
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаракты 1 категории сложности без имплонтации ИОЛ
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаракты 1 катсти без имплонтации ИОЛ Abbott Tecnis
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмуложности с имплонтацией ИОЛ Acrysof Restor Toric
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаракты 2 категории сложности без имплонтации ИОЛ льсификация катаракты категории сльсификация катаракты категории с
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация сложности без имплонтации ИОЛ Abbott Tecnis
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаракты катаракты 1 категории
                                                    </li>
                                                    <li class="item-menu-second-level">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаракты 1 категории сложности без имплонтации ИОЛ
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаракты 1 категории сложности без имплонтации ИОЛ Abbott Tecnis
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаракты категории слsadsadasd asd asd asd ожности с имплонтацией ИОЛ Acrysof Restor Toric
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаракты 2 категории сложности без имплонтации ИОЛ
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаракты 1 категории сложности без имплонтации ИОЛ Abbott Tecnis
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаракты категории сложности с имплонтацией ИОЛ Acrysof Restor Toric
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаракты 1 категории сложности без имплонтации ИОЛ
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаракты 1 категории сложности без имплонтации ИОЛ Abbott Tecnis
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаракты категории сложности с имплонтацией ИОЛ Acrysof Restor Toric
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаракты 2 категории сложности без имплонтации ИОЛ
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаракты 1 категории сложности без имплонтации ИОЛ Abbott Tecnis
                                                        </a>
                                                    </li>
                                                    <li class="item-menu-second-level">
                                                        <a href="#" class="item-menu-second-level link">
                                                            Факоэмульсификация катаракты категории сложности с имплонтацией ИОЛ Acrysof Restor Toric
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="item-menu-first-level">
                                            <a href="ServicesCommonPage?test#other-services-and-operations" class="item-menu-first-level link">
                                                Прочие операции и услуги
                                            </a>
                                            <div class="divider-list-first-level"></div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="main-menu-title js-open-menu-first-level">
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <a href="AboutPage?test" class="main-menu-title">
                                    О клинике
                                    <span class="icon-arrow g-icon icon-drop-down-arrow"></span>
                                    <div class="divider-main-menu"></div>
                                </a>
                                <div class="container-menu-first-level js-menu-first-level">
                                    <ul class="list-menu-first-level">
                                        <li class="item-menu-first-level">
                                            <a href="AboutPage?test" class="item-menu-first-level link">
                                                О клинике
                                            </a>
                                            <div class="divider-list-first-level"></div>
                                        </li>
                                        <li class="item-menu-first-level">
                                            <a href="#" class="item-menu-first-level link">
                                                Запись на прием
                                            </a>
                                            <div class="divider-list-first-level"></div>
                                        </li>
                                        <li class="item-menu-first-level">
                                            <a href="GalleryPage?test" class="item-menu-first-level link">
                                                Галерея
                                            </a>
                                            <div class="divider-list-first-level"></div>
                                        </li>
                                        <li class="item-menu-first-level">
                                            <a href="NewsCommonPage?test" class="item-menu-first-level link">
                                                Новости и статьи
                                            </a>
                                            <div class="divider-list-first-level"></div>
                                        </li>
                                        <li class="item-menu-first-level">
                                            <a href="FAQPage?test" class="item-menu-first-level link">
                                                Вопросы и ответы
                                            </a>
                                            <div class="divider-list-first-level"></div>
                                        </li>
                                        <li class="item-menu-first-level">
                                            <a href="LegalInformationPage?test" class="item-menu-first-level link">
                                                Правовая информация
                                            </a>
                                            <div class="divider-list-first-level"></div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="main-menu-title js-open-menu-first-level">
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <a href="#" class="main-menu-title">
                                    Контакты
                                    <div class="divider-main-menu"></div>
                                </a>
                            </div>
                        </div>
                        <div class="main-menu-title js-open-menu-first-level">
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <input type="search" class="input-search" placeholder="Поиск">
                                <span class="g-icon icon-search"></span>
                                <div class="divider-main-menu search"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/popups/callback-popup.php' ?>
<? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/popups/enroll-popup.php' ?>
