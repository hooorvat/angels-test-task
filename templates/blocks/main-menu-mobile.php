<div class="menu-section js-menu-for-phone">
    <a href="#" class="logo">
        <span class="g-icon icon-logo"></span>
    </a>
    <div class="menu js-open-menu-first-level">
        <span class="g-icon icon-open-menu"></span>
    </div>
    <div class="b-menu container-for-phone">
        <div class="menu-for-phone js-menu-first-level">
            <div class="content-menu ">
<!--                <div class="menu-mobile-top-container">-->
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="title">Меню</div>
                                <a href="/MainPage?test" class="g-icon icon-logo logo"></a>
                                <div class="g-icon icon-comeback-menu come-back-menu js-comeback-menu"></div>
                                <div class="g-icon icon-close close-menu js-open-menu-first-level"></div>
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="menu-mobile-menu-list-container">-->
<!--                    <div class="container">-->
<!--                        <div class="row">-->
<!--                            <div class="col-xs-4">-->
                                <ul class="list-menu">
                                    <li class="item-menu-first-level">
                                        <div class="menu-mobile-menu-item-title js-popup-open" data-target="enrollPopup">
                                            Записаться на консультацию
                                        </div>
                                    </li>
                                    <li class="item-menu-first-level  js-open-menu-second-level">
                                        <div class="menu-mobile-menu-item-title">
                                            Услуги
                                            <span class="g-icon icon-right-arrow"></span>
                                        </div>
                                        <div class="second-level-menu js-menu-second-level">
                                            <div class="content-menu ">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-xs-4">
                                                            <div class="title">Услуги</div>
                                                            <div class="divider"></div>
                                                            <ul class="list-menu">
                                                                <li class="item-menu-second-level">
                                                                    <a href="/Discounts?test" class="item-menu-second-level link">
                                                                        Акции и скидки
                                                                    </a>
                                                                </li>
                                                                <li class="item-menu-second-level js-open-menu-third-level">
                                                                    <div class="menu-mobile-menu-item-title">
                                                                        Диагностика
                                                                        <span class="g-icon icon-right-arrow"></span>
                                                                    </div>
                                                                    <div class="third-level-menu js-menu-third-level">
                                                                        <div class="content-menu ">
                                                                            <div class="container">
                                                                                <div class="row">
                                                                                    <div class="col-xs-4">
                                                                                        <div class="title">Диагностика</div>
                                                                                        <div class="divider"></div>
                                                                                        <ul class="list-menu">
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Alcon Acrysof IQ
                                                                                                </a>
                                                                                            </li><li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb
                                                                                                    Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Alcon Acrysof IQ
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="item-menu-second-level  js-open-menu-third-level">
                                                                    <div class="menu-mobile-menu-item-title">
                                                                        Лечение катаркты
                                                                        <span class="g-icon icon-right-arrow"></span>
                                                                    </div>
                                                                    <div class="third-level-menu js-menu-third-level">
                                                                        <div class="content-menu ">
                                                                            <div class="container">
                                                                                <div class="row">
                                                                                    <div class="col-xs-4">
                                                                                        <div class="title">Диагностика</div>
                                                                                        <div class="divider"></div>
                                                                                        <ul class="list-menu">
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Alcon Acrysof IQ
                                                                                                </a>
                                                                                            </li><li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb
                                                                                                    Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Alcon Acrysof IQ
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="item-menu-second-level">
                                                                    <div class="menu-mobile-menu-item-title">
                                                                        Лечение глаукомы
                                                                        <span class="g-icon icon-right-arrow"></span>
                                                                    </div>
                                                                    <div class="third-level-menu js-menu-third-level">
                                                                        <div class="content-menu ">
                                                                            <div class="container">
                                                                                <div class="row">
                                                                                    <div class="col-xs-4">
                                                                                        <div class="title">Диагностика</div>
                                                                                        <div class="divider"></div>
                                                                                        <ul class="list-menu">
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Alcon Acrysof IQ
                                                                                                </a>
                                                                                            </li><li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb
                                                                                                    Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Alcon Acrysof IQ
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="item-menu-second-level  js-open-menu-third-level">
                                                                    <div class="menu-mobile-menu-item-title">
                                                                        Востановительное лечение глаз
                                                                        <span class="g-icon icon-right-arrow"></span>
                                                                    </div>
                                                                    <div class="third-level-menu js-menu-third-level">
                                                                        <div class="content-menu ">
                                                                            <div class="container">
                                                                                <div class="row">
                                                                                    <div class="col-xs-4">
                                                                                        <div class="title">Диагностика</div>
                                                                                        <div class="divider"></div>
                                                                                        <ul class="list-menu">
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Alcon Acrysof IQ
                                                                                                </a>
                                                                                            </li><li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb
                                                                                                    Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Alcon Acrysof IQ
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="item-menu-second-level  js-open-menu-third-level">
                                                                    <div class="menu-mobile-menu-item-title">
                                                                        Прочие операции и услуги
                                                                        <span class="g-icon icon-right-arrow"></span>
                                                                    </div>
                                                                    <div class="third-level-menu js-menu-third-level">
                                                                        <div class="content-menu ">
                                                                            <div class="container">
                                                                                <div class="row">
                                                                                    <div class="col-xs-4">
                                                                                        <div class="title">Диагностика</div>
                                                                                        <div class="divider"></div>
                                                                                        <ul class="list-menu">
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Alcon Acrysof IQ
                                                                                                </a>
                                                                                            </li><li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb
                                                                                                    Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Alcon Acrysof IQ
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item-menu-first-level">
                                        <a href="/KidsServicesPage?test" class="item-menu-first-level link">
                                            Детское отделение
                                        </a>
                                    </li>
                                    <li class="item-menu-first-level js-open-menu-second-level">
                                        <div class="menu-mobile-menu-item-title">
                                            О клинике
                                            <span class="g-icon icon-right-arrow"></span>
                                        </div>
                                        <div class="second-level-menu js-menu-second-level">
                                            <div class="content-menu ">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-xs-4">
                                                            <div class="title">Услуги</div>
                                                            <div class="divider"></div>
                                                            <ul class="list-menu">
                                                                <li class="item-menu-second-level">
                                                                    <a href="/Discounts?test" class="item-menu-second-level link">
                                                                        Акции и скидки
                                                                    </a>
                                                                </li>
                                                                <li class="item-menu-second-level js-open-menu-third-level">
                                                                    <div class="menu-mobile-menu-item-title">
                                                                        Диагностика
                                                                        <span class="g-icon icon-right-arrow"></span>
                                                                    </div>
                                                                    <div class="third-level-menu js-menu-third-level">
                                                                        <div class="content-menu ">
                                                                            <div class="container">
                                                                                <div class="row">
                                                                                    <div class="col-xs-4">
                                                                                        <div class="title">Диагностика</div>
                                                                                        <div class="divider"></div>
                                                                                        <ul class="list-menu">
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Alcon Acrysof IQ
                                                                                                </a>
                                                                                            </li><li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb
                                                                                                    Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Alcon Acrysof IQ
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="item-menu-second-level  js-open-menu-third-level">
                                                                    <div class="menu-mobile-menu-item-title">
                                                                        Лечение катаркты
                                                                        <span class="g-icon icon-right-arrow"></span>
                                                                    </div>
                                                                    <div class="third-level-menu js-menu-third-level">
                                                                        <div class="content-menu ">
                                                                            <div class="container">
                                                                                <div class="row">
                                                                                    <div class="col-xs-4">
                                                                                        <div class="title">Диагностика</div>
                                                                                        <div class="divider"></div>
                                                                                        <ul class="list-menu">
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Alcon Acrysof IQ
                                                                                                </a>
                                                                                            </li><li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb
                                                                                                    Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Alcon Acrysof IQ
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="item-menu-second-level">
                                                                    <div class="menu-mobile-menu-item-title">
                                                                        Лечение глаукомы
                                                                        <span class="g-icon icon-right-arrow"></span>
                                                                    </div>
                                                                    <div class="third-level-menu js-menu-third-level">
                                                                        <div class="content-menu ">
                                                                            <div class="container">
                                                                                <div class="row">
                                                                                    <div class="col-xs-4">
                                                                                        <div class="title">Диагностика</div>
                                                                                        <div class="divider"></div>
                                                                                        <ul class="list-menu">
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Alcon Acrysof IQ
                                                                                                </a>
                                                                                            </li><li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb
                                                                                                    Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Alcon Acrysof IQ
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="item-menu-second-level  js-open-menu-third-level">
                                                                    <div class="menu-mobile-menu-item-title">
                                                                        Востановительное лечение глаз
                                                                        <span class="g-icon icon-right-arrow"></span>
                                                                    </div>
                                                                    <div class="third-level-menu js-menu-third-level">
                                                                        <div class="content-menu ">
                                                                            <div class="container">
                                                                                <div class="row">
                                                                                    <div class="col-xs-4">
                                                                                        <div class="title">Диагностика</div>
                                                                                        <div class="divider"></div>
                                                                                        <ul class="list-menu">
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Alcon Acrysof IQ
                                                                                                </a>
                                                                                            </li><li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb
                                                                                                    Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Alcon Acrysof IQ
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="item-menu-second-level  js-open-menu-third-level">
                                                                    <div class="menu-mobile-menu-item-title">
                                                                        Прочие операции и услуги
                                                                        <span class="g-icon icon-right-arrow"></span>
                                                                    </div>
                                                                    <div class="third-level-menu js-menu-third-level">
                                                                        <div class="content-menu ">
                                                                            <div class="container">
                                                                                <div class="row">
                                                                                    <div class="col-xs-4">
                                                                                        <div class="title">Диагностика</div>
                                                                                        <div class="divider"></div>
                                                                                        <ul class="list-menu">
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Alcon Acrysof IQ
                                                                                                </a>
                                                                                            </li><li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb
                                                                                                    Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Alcon Acrysof IQ
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="item-menu-third-level">
                                                                                                <a href="#" class="item-menu-third-level link">
                                                                                                    Факоэмульсификация катаракты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                                                                                </a>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item-menu-first-level">
                                        <a href="/ContactsPage?test" class="item-menu-first-level link">
                                            Контакты
                                        </a>
                                    </li>
                                    <li class="item-menu-first-level">
                        <span class="item-menu-first-level link">
                            <input class="search-input" type="text" placeholder="Поиск">
                            <span class="g-icon icon-search"></span>
                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
<!--                </div>-->
                <div class="menu-mobile-callback-button-container">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="menu-mobile-callback-button">
                                    <a href="tel:84732622233" class="phone">8 473 262-22-33 </a>
                                    <a href="#" class="link">| Перезвонить</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>