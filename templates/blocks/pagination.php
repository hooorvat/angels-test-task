<div class="row">
    <div class="col-xs-4 col-sm-12 col-md-12 col-lg-12">
        <div class="slider-controls">
            <div class="previous-button"></div>
            <div class="slider-bulls-container col-sm-offset-3 col-md-offset-3 col-lg-offset-3">
                <div class="bull js-bull">
                    1
                </div>
                <div class="bull js-bull">
                    2
                </div>
                <div class="bull js-bull">
                    3
                </div>
                <div class="bull js-bull">
                    4
                </div>
                <div class="bull js-bull">
                    5
                </div>
            </div>
            <div class="slider-next-button"></div>
        </div>
    </div>
</div>