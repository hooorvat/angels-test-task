<div class="p-footer">
    <div class="yandex-map js-yandex-map">
        <div class="map js-map"  data-coordinates="[51.676234982712494, 39.203203042327864]"></div>
        <div class="map-exit-button js-exit-button">
            <svg height="20" width="20">
                <line x1="0" y1="0" x2="20" y2="20" style="stroke:#FFFFFF; stroke-width:2"/>
                <line x1="20" y1="0" x2="0" y2="20" style="stroke:#FFFFFF; stroke-width:2"/>
            </svg>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="container-xs col-sm-hide col-md-hide col-lg-hide">
                <div class="col-xs-4">
                    <div class="footer-menu">
                        <a href="#" class="menu-item">
                            О центре
                        </a>
                        <a href="#" class="menu-item">
                            Услуги
                        </a>
                        <a href="#" class="menu-item">
                            Контакты
                        </a>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="divider"></div>
                </div>
                <div class="footer-menu info">
                    <div class="col-xs-4">
                        <div class="info-section">
                            <div class="info-text">
                                Время рботы по будням: с 8:00 до 20:00; <br>
                                В субботу с 9:00 до 17:00;
                            </div>
                            <div class="title">
                                Адрес:
                            </div>
                            <div class="info-text">
                                394036, г. Воронеж, ул. Студенческая, 12а,
                                вход со стороны ул. Студенческая.
                            </div>
                            <a href="#" class="link-footer js-map-popup">
                                Показать карту проезда
                            </a>
                            <div class="title">
                                Телефоны
                            </div>
                            <a href="tel:7473-262-22-33" class="telephone">
                                +7 473 262-22-33;
                            </a>
                            <a href="tel:+7 473 262-22-17" class="telephone">
                                +7 473 262-22-17
                            </a>
                            <a href="tel:7473-262-22-33" class="link-footer" data-target="callbackPopup">
                                Перезвонить
                            </a>
                            <div class="title">
                                Электронная почта:
                            </div>
                        </div>
                    </div>
                    <div class="info-section">
                        <div class="col-xs-2">
                            <a href="mailto:mail@oftalmolog36.ru" class="link-footer">
                                mail@oftalmolog36.ru
                            </a>
                        </div>
                    </div>
                    <div class="info-section text-align-right">
                        <div class="col-xs-2">
                            <a href="#" class="link-footer" data-target="askQuestionPopup">
                                Написать нам
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="divider"></div>
                </div>
                <div class="col-xs-4">
                    <div class="social-networks">
                        <a href="#" class="network-item">
                            Vkontakte
                        </a>
                        <a href="#" class="network-item">
                            Facebook
                        </a>
                        <a href="#" class="network-item">
                            Youtube
                        </a>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="divider"></div>
                </div>
                <div class="col-xs-4">
                    <div class="copyright">
                        <div class="text-copyright">
                            © 2016 OOO ЦКО «Мединвест» <br>
                            —Центр клинической офтальмологи.
                        </div>
                        <div class="text-copyright">
                            <br>
                            <br>
                            Дизайн и разработка сайта:
                            <a href="#" class="link-copyright">
                                <br>
                                <+> Doctornet.pro
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-lg col-xs-hide">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 col-md-3 col-lg-3 js-hide-when-map">
                    <a href="./MainPage?test" class="logo">
                        <div class="g-icon icon-large-logo"></div>
                    </a>
                    <div class="social-networks">
                        <div class="social-networks-title">
                            В социальных сетях:
                        </div>
                        <ul>
                            <li class="network-item">
                                <a href="https://vk.com/" class="network-item">
                                    <i class="g-icon icon-vk"></i>
                                    ВКонтакте
                                </a>
                            </li>
                            <li class="network-item">
                                <a href="https://ok.ru/" class="network-item">
                                    <i class="g-icon icon-ok"></i>
                                    Одноклассники
                                </a>
                            </li>
                            <li class="network-item">
                                <a href="https://facebook.com/" class="network-item">
                                    <i class="g-icon icon-fb"></i>
                                    Facebook
                                </a>
                            </li>
                            <li class="network-item" >
                                <a href="https://www.youtube.com/" class="network-item">
                                    <i class="g-icon icon-yt"></i>
                                    Youtube
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-2 col-md-2 col-lg-2">
                    <div class="footer-menu">
                        <a href="./AboutPage?test" class="menu-item">
                            О центре
                        </a>
                        <ul class="link-list-lg col-xs">
                            <li class="link-list-item">
                                <a href="./SpecialistsPage?test" class="link-lg">
                                    Наши специалисты
                                </a>
                            </li>
                            <li class="link-list-item">
                                <a href="./FAQPage?test" class="link-lg">
                                    Вопрос / Ответ
                                </a>
                            </li>
                            <li class="link-list-item">
                                <a href="./TestimonislsPage?test" class="link-lg">
                                    Отзывы
                                </a>
                            </li>
                            <li class="link-list-item">
                                <a href="./NewsCommonPage?test" class="link-lg">
                                    Новости
                                </a>
                            </li>
                            <li class="link-list-item">
                                <a href="./GalleryPage?test" class="link-lg">
                                    Блог центра
                                </a>
                            </li>
                            <li class="link-list-item">
                                <a href="./LegalInformationPage?test" class="link-lg">
                                    Контролирующие <br> органы
                                </a>
                            </li>
                            <li class="link-list-item">
                                <a href="#" class="link-lg">
                                    Терминологический <br> словарь
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-2 col-md-2 col-lg-2">
                    <div class="footer-menu">
                        <a href="./ServicesCommonPage?test" class="menu-item">
                            Услуги
                        </a>
                        <ul class="link-list-lg col-xs">
                            <li class="link-list-item">
                                <a href="./ServicesCommonPage?test#diagnostic" class="link-lg">
                                    Диагностика
                                </a>
                            </li>
                            <li class="link-list-item">
                                <a href="./ServicesCommonPage?test#cataract-cure" class="link-lg">
                                    Лечение катараты
                                </a>
                            </li>
                            <li class="link-list-item">
                                <a href="./ServicesCommonPage?test#glaucoma-cure" class="link-lg">
                                    Лечение глаукомы
                                </a>
                            </li>
                            <li class="link-list-item">
                                <a href="./ServicesCommonPage?test#retina-cure" class="link-lg">
                                    Лазеркоагуляция сетчатки
                                </a>
                            </li>
                            <li class="link-list-item">
                                <a href="./ServicesCommonPage?test#restoring-cure" class="link-lg">
                                    Востановительное лечение глаз
                                </a>
                            </li>
                            <li class="link-list-item">
                                <a href="./ServicesCommonPage?test#other-services-and-operations" class="link-lg">
                                    Прочие услуги
                                </a>
                            </li>
                            <li class="link-list-item">
                                <a href="#" class="link-lg">
                                    Скидки и акции
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-5 col-md-5 col-lg-5">
                    <div class="footer-menu info">
                        <a href="./ContactsPage?test" class="menu-item">
                            Контакты
                        </a>
                        <div class="info-section">
                            <div class="info-text">
                                Время работы по будням: с 8:00 до 20:00; <br>
                                В субботу с 9:00 до 17:00;
                            </div>
                            <div class="title">
                                Адрес:
                            </div>
                            <div class="info-text">
                                394036, г. Воронеж, ул. Студенческая, 12а,
                                вход со стороны ул. Студенческая.
                            </div>
                            <a class="link-footer js-map-popup">
                                Показать карту проезда
                            </a>
                            <div class="title">
                                Телефоны
                            </div>
                            <a href="tel:7473-262-22-33" class="telephone">
                                +7 473 262-22-33;
                            </a>
                            <a href="tel:+7 473 262-22-17" class="telephone">
                                +7 473 262-22-17
                            </a>
                            <a href="#" class="link-footer js-popup-open" data-target="callbackPopup">
                                Перезвонить
                            </a>
                            <div class="title">
                                Электронная почта:
                            </div>
                            <a href="mailto:mail@oftalmolog36.ru" class="link-footer">
                                mail@oftalmolog36.ru
                            </a>
                            <a href="#" class="link-footer space js-popup-open" data-target="askQuestionPopup">
                                Написать нам
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="divider"></div>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="copyright">
                        <span class="text-copyright">
                            © 2016 OOO ЦКО «Мединвест» — Центр клинической офтальмологи.
                        </span>
                        <span class="text-copyright">
                                Дизайн и разработка сайта:
                            <span class="space-copyright"></span>
                            <a href="http://doctornet.pro/" target="_blank" class="link-copyright"><+> Doctornet.pro</a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/popups/ask-question-popup.php' ?>
</div>



<script src="<?= $url_prefix ?>/assets/common.js"></script>
<script src="<?= $url_prefix ?>/assets/main.js"></script>
</body>
</html>
