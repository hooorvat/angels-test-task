<body>
<div class="p-header service-page kids">
    <div class="col-xs col-sm-hide col-md-hide col-lg-hide image-overlay" style="background-image: url('../../site/src/images/images-fish/background-header.png')">
        <div class="image-background">
            <div class="container">
                <div class="row">
                    <? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/blocks/main-menu-mobile.php' ?>
                </div>
                <div class="header-service-title-container">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-4">
                                <? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/blocks/header-breadcrumbs.php' ?>
                            </div>
                            <div class="col-xs-4">
                                <h1 class="title-header-mobile">
                                    Факоэмульсификация катаракты
                                </h1>
                                <div class="decription-header-mobile">
                                    2я категория сложности с имплантацией монофакальной ИОЛ Bausch+Lomb Envista
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-hide" style="height: 100%">
        <? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/blocks/fixed-menu.php' ?>
        <div class="js-menu image-overlay" style="background-image: url('../../site/src/images/images-fish/kids-services-bg.png')">
            <div class="image-background">
                <div class="container container-lg">
                    <div class="row">
                        <? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/blocks/main-menu-desktop-kids.php' ?>
                        <div class="col-sm-3 col-md-3 col-lg-2">
                            <? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/blocks/header-breadcrumbs.php' ?>
                        </div>
                        <div class="col-sm-9 col-md-9 col-lg-9 col-lg-offset-1">
                            <div class="title-header">
                                Лазерная операция ППЛК
                            </div>
                            <div class="description-header">
                                Переферическая профилактическая лазеркоагуляция сетчатки
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <? if ($_SERVER['REQUEST_URI'] == '/KidsServicesCommonPage?dev') {
            echo '
                <div class="header-bottom-container">
                    <div class="container">
                        <div class="header-bottom-content-wrapper">
                            <div class="row">
                                <div class="col-sm-3 col-md-3 col-lg-3">
                                    <img alt="" class="roll-down-icon" src="' .  $php_prefix . './site/src/images/images-static/icon/page-down-icon-kids.svg">
                                    <a href="#kids-services-content" class="page-down-button">
                                        Подробности ниже
                                    </a>
                                </div>
                                <div class="col-sm-3 col-md-3 col-lg-3">
                                    <a href="#" class="info-link js-popup-open" data-target="enrollPopup">
                                        Записаться на приём
                                    </a>
                                </div>
                                <div class="col-sm-3 col-md-3 col-lg-3">
                                    <div class="service-page-video-link-wrapper">
                                        <i class="g-icon icon-play"></i>
                                        <a class="video-link js-popup-open" data-target="videoPopup">
                                            Видео о работе отделения
                                        </a>
                                    </div>
                                </div>
                                <img class="chipmunk-image" src="' . $php_prefix . './site/src/images/images-static/kids-services-chipmunk.png" alt="">';
                                require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/popups/video-popup.php';
                            echo '</div>
                        </div>
                    </div>
                </div>';
            } else {
                echo '
                    <div class="header-bottom-container">
                        <div class="container">
                            <div class="row">
                                <div class="header-bottom-content-wrapper">
                                    <div class="col-sm-3 col-md-3 col-lg-3">
                                        <div class="cost-services-title">
                                            Стоимость услуги
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2 col-lg-2">
                                        <div class="price">
                                            7 000
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-md-3 col-lg-3">
                                        <a href="#" class="info-link js-popup-open" data-target="enrollPopup">
                                            Записаться на приём
                                        </a>
                                    </div>
                                    <div class="col-sm-4 col-md-4 col-lg-4">
                                        <div class="service-page-video-link-wrapper">
                                            <i class="g-icon icon-play"></i>
                                            <a class="video-link js-popup-open" data-target="videoPopup">
                                                Показать видео об услуге
                                            </a>
                                        </div>
                                    </div>';
                                    require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/popups/video-popup.php';
                                echo '</div>
                            </div>
                        </div>
                    </div>';
        }
        ?>
    </div>
    <? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/popups/video-popup.php' ?>
</div>