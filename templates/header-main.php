<body>
<div class="p-header main-page-header">
    <div class="col-sm-hide col-md-hide col-lg-hide image-overlay" style="background-image: url('../../site/src/images/images-static/main-page-bg.jpg'); background-position: center center;
    background-repeat: no-repeat;">
        <svg xmlns="http://www.w3.org/2000/svg" width="100vw" height="100%" style="position: absolute; top: 0;">
            <defs>
                <mask id="mask-mobile">
                    <rect width="100%" height="100%" fill="#fff" />
                    <svg class="svg-mask js-svg-mask" xmlns="http://www.w3.org/2000/svg" y="50" width="100%" viewBox="0 200 530 130">
                        <polygon points="0 125 30 125 30 35 90 35 90 125 120 125 120 5 0 5 0 125"/>
                        <path d="M210,0a65,65,0,1,0,65,65A65,65,0,0,0,210,0Zm0,100a35,35,0,1,1,35-35A35,35,0,0,1,210,100Z"/>
                        <polygon transform="translate(0,150)" points="215.02 40.925 185.02 5.015 155.02 5.015 155.02 125.015 185.02 125.015 185.02 55.015 208.59 85.015 221.45 85.015 245.02 55.015 245.02 85.015 245.02 85.015 245.02 115.015 245.02 115.015 245.02 125.015 275.02 125.015 275.02 5.015 245.02 5.015 215.02 40.925"/>
                        <path transform="translate(0,150)" d="M383.59033,62.50475l-25.35-15.12a2.82,2.82,0,0,0-4.22,2.52v30.23a2.82,2.82,0,0,0,4.23,2.52l25.35-15.12a3,3,0,0,0-.00995-5.03Z"/>
                        <path transform="translate(0,150)" d="M65.02032,30.01475a35,35,0,0,1,31.63,20h31.61a65,65,0,1,0,0,30h-31.61a35,35,0,1,1-31.63-50Z"/>
                        <path transform="translate(0,150)" d="M365.02032.01475a65,65,0,1,0,65,65A65,65,0,0,0,365.02032.01475Zm0,100a35,35,0,1,1,35-35A35,35,0,0,1,365.02032,100.01475Z"/>
                        <path transform="translate(0,300)" d="M225,0H150V120h30V90h45a45,45,0,0,0,0-90Zm0,60H180V30h45a15,15,0,0,1,0,30Z"/>
                        <polygon transform="translate(0,300)" points="385 0 330 75 330 0 300 0 300 120 335 120 390 45 390 120 420 120 420 0 385 0"/>
                        <polygon transform="translate(0,300)" points="0 30 45 30 45 120 75 120 75 30 120 30 120 0 0 0 0 30"/>
                    </svg>
                </mask>
            </defs>
            <rect class="mask-wrapper" width="100%" height="100%" fill="#003366" fill-opacity="1" mask="url(#mask-mobile)" />
        </svg>
        <div class="container container-xs" style="padding-bottom: 30px">
            <div class="row">
                <? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/blocks/main-menu-mobile.php' ?>
                <div class="col-xs-4">
                    <h1 class="title-header">
                        Центр<br>Клинической<br>Офтальмологии
                    </h1>
                </div>
            </div>
        </div>
        <div class="header-bottom-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-4 col-sm-12 col-md-12 col-lg-12">
                        <div class="header-bottom-content-wrapper">
                            <a class="video-link">
                                <i class="video-link-icon"></i>
                                Видеоролик о центре
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-hide image-overlay js-menu" style="background-image: url('../../site/src/images/images-static/main-page-bg.jpg');">
        <? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/blocks/fixed-menu.php' ?>
        <? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/blocks/main-menu-desktop.php' ?>
        <svg class="js-popup-open" data-target="videoPopup" xmlns="http://www.w3.org/2000/svg" width="100vw" height="100%" style="position: absolute; top: 0;">
            <defs>
                <mask id="mask-desktop">
                    <rect width="100%" height="100%" fill="#fff" />
                    <svg class="svg-mask js-svg-mask" xmlns="http://www.w3.org/2000/svg" y="210" width="100%" viewBox="0 200 1170 130">
                        <polygon points="510 40.91 480 5 450 5 450 125 480 125 480 55 503.57 85
                            516.43 85 540 55 540 85 540 85 540 115 540 115 540 125 570 125 570 5 540 5 510 40.91"/>
                        <path d="M678.57,62.49,653.22,47.37A2.82,2.82,0,0,0,649,49.89V80.12a2.82,2.82,
                            0,0,0,4.23,2.52l25.35-15.12A3,3,0,0,0,678.57,62.49Z"/>
                        <polygon points="0 125 30 125 30 35 90 35 90 125 120 125 120 5 0 5 0 125"/>
                        <path d="M360,30a35,35,0,0,1,31.63,20h31.61a65,65,0,1,0,0,30H391.63A35,35,0,1,1,360,30Z"/>
                        <circle cx="210" cy="65" r="65"/>
                        <path d="M975,5H900V125h30V95h45a45,45,0,0,0,0-90Zm0,60H930V35h45a15,15,0,1,1,0,30Z"/>
                        <polygon points="1135 5 1080 80 1080 5 1050 5 1050 125 1085 125 1140 50 1140 125 1170 125 1170 5 1135 5"/>
                        <path d="M660,0a65,65,0,1,0,65,65A65,65,0,0,0,660,0Zm0,100a35,35,0,1,1,35-35A35,35,0,0,1,660,100Z"/>
                        <polygon points="750 35 795 35 795 125 825 125 825 35 870 35 870 5 750 5 750 35"/>
                    </svg>
                </mask>
            </defs>
            <rect class="mask-wrapper" width="100%" height="100%" fill="#003366" fill-opacity="0.5" mask="url(#mask-desktop)" />
        </svg>
        <div class="container container-lg" >
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="slogan">Как прекрасен этот мир</div>
                </div>
            </div>
        </div>
        <div class="header-bottom-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-4 col-sm-12 col-md-12 col-lg-12">
                        <div class="header-bottom-content-wrapper">
                            <div class="row">
                                <div class="col-sm-3 col-md-3 col-lg-3">
                                    <a href="#main-page-content" class="page-down-button"><img alt="" src="<?= $php_prefix ?>./site/src/images/images-static/icon/page-down-icon.svg"></a>
                                </div>
                                <div class="col-sm-3 col-md-3 col-lg-3">
                                    <i class="g-icon icon-play"></i>
                                    <a class="video-link js-popup-open" data-target="videoPopup">
                                        Видеоролик о центре
                                    </a>
                                </div>
                                <div class="col-sm-3 col-md-3 col-lg-3">
                                    <i class="g-icon icon-gallery"></i>
                                    <a href="Gallery?test" class="gallery-link">
                                        Фото и видео
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/popups/video-popup.php' ?>
</div>