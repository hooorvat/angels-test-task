<body>
<div class="p-header white">
    <div class="col-sm-hide col-md-hide col-lg-hide image-overlay">
        <div class="container container-xs">
            <div class="row">
                <? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/blocks/main-menu-mobile.php' ?>
                <div class="col-xs-4">
                    <? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/blocks/header-breadcrumbs.php' ?>
                </div>
                <div class="col-xs-4">
                    <h1 class="title-header">
                        Заголовок страницы
                    </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-hide js-menu image-overlay">
        <? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/blocks/fixed-menu.php' ?>
        <div class="container container-lg">
            <div class="row">
                <? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/blocks/main-menu-desktop.php' ?>
                <div class="col-sm-3 col-md-3 col-lg-3">
                    <? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/blocks/header-breadcrumbs.php' ?>
                </div>
                <div class="col-sm-9 col-md-9 col-lg-9">
                    <h1 class="title-header">
                        Заголовок страницы
                    </h1>
                </div>
            </div>
        </div>
    </div>
</div>