<?
$isBack = false;

if (isset($_GET['dev'])) {
    $url_prefix = 'http://localhost:8080';
    $frontEndPostfix = '?dev';
    $php_prefix = '';
} elseif (isset($_GET['test'])) {
    $url_prefix = '/test';
    $frontEndPostfix = '?test';
    $php_prefix = '';
} else {
    $url_prefix = '/assets/';
    $php_prefix = '/assets/';
    $isBack     = true;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Medinvest</title>
    <link rel="stylesheet" type="text/css" href="<?= $url_prefix ?>/assets/main.css">

    <meta property="og:url" content="http://www.your-domain.com/your-page.html"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Your Website Title"/>
    <meta property="og:description" content="Your description"/>
    <meta property="og:image" content="http://www.your-domain.com/path/image.jpg"/>
    <link rel="icon" type="image/vnd.microsoft.icon" href="./data/images/favicon.ico"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
</head>