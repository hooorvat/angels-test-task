<? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/header-white.php' ?>
<div class="p-content">
    <div class="container">
        <div class="b-contacts">
            <div class="row">
                <div class="contacts">
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <div class="section-title">
                            <h2 class="title">
                                ЦКО Мединвест
                            </h2>
                            <div class="description">
                                Взрослое и детское отделения
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-7 col-md-7 col-lg-7">
                        <div class="sections">
                            <div class="section">
                                <div class="address">
                                    <h3 class="title g-third-heading">
                                        Адрес
                                    </h3>
                                    <p class="content g-paragraph">394036, Россия, <br> г.Воронеж, ул. Студенческая, 12а. <br>
                                        Вход со стороны ул. Студенческая
                                    </p>
                                </div>
                            </div>
                            <div class="section">
                                <div class="time">
                                    <h3 class="title g-third-heading">
                                        Время работы
                                    </h3>
                                    <p class="content g-paragraph">
                                        По будням: с 8:00 до 20:00; <br> В субботу: с 9:00 до 17:00; <br>
                                        Воскресенье - выходной.
                                    </p>
                                </div>
                            </div>
                            <div class="section">
                                <div class="telephones">
                                    <h3 class="title g-third-heading">Телефоны</h3>
                                    <p class="content g-paragraph">+7 (473) 262-22-33 - регистратура; <br> +7 (473) 262-22-17 - секретарь.</p>
                                    <a href="#" class="link js-popup-open" data-target="callbackPopup">Перезвонить</a>
                                </div>
                            </div>
                            <div class="section">
                                <div class="email">
                                    <h3 class="title g-third-heading">Электронная почта</h3>
                                    <a href="mailto:mail@oftalmplog36.ru" class="email-link">mail@oftalmolog36.ru</a><br>
                                    <a href="#" class="link">Написать нам</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-4 col-sm-12 col-md-12 col-lg-12">
                    <div class="map-container">
                        <div class="yandex-map js-yandex-map">
                            <div class="map js-map"  data-coordinates="[51.676234982712494, 39.203203042327864]"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="feedback">
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <div class="section-title">
                            <h2 class="title">
                                Обратная связь
                            </h2>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-hide col-md-hide col-lg-hide">
                        <div class="feedback-form js-form">
                            <form class="f-form">
                                <div class="f-field">
                                    <span class="f-message"></span>
                                    <input class="f-input" type="text" name="name" required>
                                    <label class="f-label">Ваше имя</label>
                                </div>
                                <div class="f-field">
                                    <span class="f-message"></span>
                                    <input class="f-input js-phone-input" type="tel" name="phone" required>
                                    <label class="f-label">Телефон</label>
                                </div>
                                <div class="f-field">
                                    <span class="f-message"></span>
                                    <input class="f-input" type="email" name="email" required>
                                    <label class="f-label">Email</label>
                                </div>
                                <div class="f-field">
                                    <span class="f-message"></span>
                                    <textarea class="f-textarea"></textarea>
                                    <label class="f-label">Сообщение</label>
                                </div>
                                <div class="f-field">
                                    <input class="f-submit" type="submit" value="Отправить сообщение">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-xs-hide col-sm-6 col-md-6 col-lg-6">
                        <div class="feedback-form js-form">
                            <form class="f-form">
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="f-field">
                                        <span class="f-message"></span>
                                        <input class="f-input" type="text" name="name" required>
                                        <label class="f-label">Ваше имя</label>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="f-field">
                                        <span class="f-message"></span>
                                        <input class="f-input js-phone-input" type="tel" name="phone" required>
                                        <label class="f-label">Телефон</label>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="f-field">
                                        <span class="f-message"></span>
                                        <input class="f-input" type="email" name="email" required>
                                        <label class="f-label">Email</label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="f-field">
                                        <span class="f-message"></span>
                                        <textarea class="f-textarea"></textarea>
                                        <label class="f-label">Сообщение</label>
                                    </div>
                                </div>
                                <div class="col-sm-6 col col-md-6 col-lg-6">
                                    <div class="f-field">
                                        <input class="f-submit" type="submit" value="Отправить сообщение">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>