<? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/header-white.php' ?>
<div class="p-content">
    <div class="container">
        <div class="b-faq">
            <div class="menu">
                <div class="row">
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <div class="select-wrapper js-select">
                            <select class="f-select">
                                <option value="hide">
                                    Все вопросы
                                </option>
                                <option value="1">
                                    Операции
                                </option>
                                <option value="2">
                                    Общие вопросы
                                </option>
                                <option value="3">
                                    Диагностика
                                </option>
                            </select>
                            <div class="arrow"></div>
                        </div>
                    </div>
                    <div class="col-xs-hide col-sm-3 col-md-3 col-lg-3">
                        <div class="ask">
                            <img class="icon" src="<?= $php_prefix ?>./site/src/images/images-static/icon/ask-question.png" alt="">
                            <div class="g-dotted-link js-popup-open" data-target="askQuestionPopup">Задать вопрос</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="questions-container">
                <div class="question">
                    <div class="row">
                        <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                            <div class="name">
                                Раушан
                            </div>
                            <div class="category">
                                Общие вопросы
                            </div>
                        </div>
                        <div class="col-xs-4 col-sm-9 col-md-9 col-lg-9">
                            <p class="question-text g-paragraph">
                                Добрый день! Ребенку 2,5 года, прошли комисию и нам сказали, что у сына астигматизм и
                                дальнозоркость на оба глаза +3. Также окулист сказал, что без очков не обойтись. У меня вопрос
                                такой: возможно ли полностью вылечить астигматизм? Может нужно делать какие то упражнения,
                                физ. лечение и т.д.? Мы проживаем в Троицке, имею ли я право попросить направление в областную
                                детскую больницу для дополнительной консультации? Заранее спасибо!
                            </p>
                            <blockquote class="g-blockquote">
                                <div class="heading">Ответ:</div>
                                Здравствуйте, Раушан. Астигматизм и дальнозоркость - это врожденные состояния, которые лечатся
                                в несколько этапов, первый из которых ношение очков. Направление вы попросить можете.
                            </blockquote>
                        </div>
                    </div>
                </div>
                <div class="question">
                    <div class="row">
                        <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                            <div class="name">
                                Ирина
                            </div>
                            <div class="category">
                                Операции
                            </div>
                        </div>
                        <div class="col-xs-4 col-sm-9 col-md-9 col-lg-9">
                            <p class="question-text g-paragraph">
                                Здравствуйте! Скажите, пожалуйста, возможно ли сделать бесплатную операцию? Дело в том,
                                что три года назад, будучи беременной, мне не разрешили рожать самостоятельно т.к.
                                сказали, что у меня миопия второй степени и дистрофия сетчатки. Операцию я тогда не
                                делала, т.к не было денег. А сейчас хотела бы все исправить. Скажите, действительно,
                                100% нужна операция?
                            </p>
                            <blockquote class="g-blockquote">
                                <div class="heading">Ответ:</div>
                                Здравствуйте, Ирина. Операция по восстановлению зрения бесплатно не проводится нигде.
                                Лазерная коагуляция сетчатки по поводу дистрофии сетчатки проводится строго по показаниям.
                                Узнайте у вашего офтальмолога можно ли провести данную операцию Вам бесплатно по направлению.
                            </blockquote>
                        </div>
                    </div>
                </div>
                <div class="question">
                    <div class="row">
                        <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                            <div class="name">
                                Константин Константинович Константинопольский
                            </div>
                            <div class="category">
                                Операции
                            </div>
                        </div>
                        <div class="col-xs-4 col-sm-9 col-md-9 col-lg-9">
                            <p class="question-text g-paragraph">
                                Сколько стоит лазерная операция по улучшению зрения на оба глаза с -5.0 -7.0 до идеала 0.0?
                                И как делается операция?
                            </p>
                            <blockquote class="g-blockquote">
                                <div class="heading">Ответ:</div>
                                Рекомендуем <span class="g-link">записаться на диагностику</span>, чтобы определить метод улучшения вашего зрения.
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>
            <? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/blocks/pagination.php' ?>
            <div class="row">
                <div class="col-xs-4 col-sm-hide col-md-hide col-lg-hide">
                    <div class="ask js-popup-open" data-target="askQuestionPopup">Задать вопрос</div>
                </div>
            </div>
        </div>
    </div>
</div>
<? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/popups/ask-question-popup.php' ?>

