<? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/header-kids.php' ?>
<div class="p-content">
    <div class="container">
        <div class="b-testimonials kids">
            <div class="row">
                <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                    <div class="section-title kids">
                        <h2 class="title kids">
                            Отзывы
                        </h2>
                        <a href="#" class="commonpage-link">
                            Посмотреть все отзывы
                        </a>
                    </div>
                </div>
                <div class="b-slider js-slider">
                    <div class="col-xs-4 col-sm-9 col-md-9 col-lg-9">
                        <div class="slider-viewport js-viewport">
                            <div class="slides-container js-slides">
                                <div class="slides-wrapper js-slides-wrapper">
                                    <div class="slide js-slide">
                                        <div class="slider-image kids">
                                            <img class="g-image" alt="" src="<?= $php_prefix ?>./site/src/images/images-fish/slider-image.jpg">
                                        </div>
                                        <div class="name kids">
                                            Страхова Н.В.
                                        </div>
                                        <div class="slider-date">
                                            22.09.2016
                                        </div>
                                        <div class="testimonial js-testimonial">
                                            Клиника супер!!!! Оборудование, дизайн, специалисты. На все вопросы я получила квалифицированные
                                            ответы (врач Богатырева Е.С.). Врач и остальной персонал доброжелательны. Когда сынок раскапризничался,
                                            врач позвала еще од сотрудника и они терпеливо развлекали моего капризюлю и гремели во все погремушки
                                            разом. Отдельно хочется отметить дизайн - очень клас.
                                            У сына аж рот открылся, глядя на яркий великолепный диван! Да, признаться, и у нас с мужем тоже!
                                        </div>
                                        <span class="overflow-message-roll-down js-roll-down g-roll-link">Развернуть</span>
                                        <span class="overflow-message-roll-up js-roll-up g-roll-link">Свернуть</span>
                                    </div>
                                    <div class="slide js-slide">
                                        <div class="slider-image kids">
                                            <img class="g-image" alt="" src="<?= $php_prefix ?>./site/src/images/images-fish/stuff-2.jpg">
                                        </div>
                                        <div class="name kids">
                                            Иванов Н.В.
                                        </div>
                                        <div class="slider-date">
                                            2.29.2014
                                        </div>
                                        <div class="testimonial js-testimonial">
                                            Повышение Квалификации: Семинар по методике преподавания английского языка на разных
                                            этапах обучения (Макмиллан, 11.03.2013) "Совершенствование иноязычного образования в
                                            школе и современные системы тестирования" Цикл семинаров с 29.10.2014 по 25.03.2013
                                            (5 часов) Oxford Professional Development Seminar. Learning the Language and Growing
                                            Professionally 08.04.2013 Занималась подготовкой участника научно-практической
                                            конференции "Мир иностранных языков" 06.04.2013(Лингвострановедение- Байконур.Казахстан)
                                            Подготовка участника конкурса эссэ (проект ЮНЕСКО 2010год) Подготовка учеников к
                                            Олимпиадам Всероссийского и регионального уровня.
                                        </div>
                                        <span class="overflow-message-roll-down js-roll-down g-roll-link">Развернуть</span>
                                        <span class="overflow-message-roll-up js-roll-up g-roll-link">Свернуть</span>
                                    </div>
                                    <div class="slide js-slide">
                                        <div class="slider-image kids">
                                            <img class="g-image" alt="" src="<?= $php_prefix ?>./site/src/images/images-fish/stuff-1.jpg">
                                        </div>
                                        <div class="name kids">
                                            Страхова Н.В.
                                        </div>
                                        <div class="slider-date">
                                            22.09.2016
                                        </div>
                                        <div class="testimonial  js-testimonial">
                                            Для них была организована небольшая экскурсия по школьному музею, где гвоздём программы
                                            была экспозиция
                                        </div>
                                        <span class="overflow-message-roll-down js-roll-down g-roll-link">Развернуть</span>
                                        <span class="overflow-message-roll-up js-roll-up g-roll-link">Свернуть</span>
                                    </div>
                                    <div class="slide js-slide">
                                        <div class="slider-image kids">
                                            <img class="g-image" alt="" src="<?= $php_prefix ?>./site/src/images/images-fish/fish-image-1.jpg">
                                        </div>
                                        <div class="name kids">
                                            Перов Н.В.
                                        </div>
                                        <div class="slider-date">
                                            27.01.2056
                                        </div>
                                        <div class="testimonial js-testimonial">
                                            Повышение Квалификации: Семинар по методике преподавания английского языка на разных
                                            этапах обучения (Макмиллан, 11.03.2013) "Совершенствование иноязычного образования в
                                            школе и современные системы тестирования" Цикл семинаров с 29.10.2014 по 25.03.2013
                                            (5 часов) Oxford Professional Development Seminar. Learning the Language and Growing
                                            Professionally 08.04.2013 Занималась подготовкой участника научно-практической
                                            конференции "Мир иностранных языков" 06.04.2013(Лингвострановедение- Байконур.Казахстан)
                                            Подготовка участника конкурса эссэ (проект ЮНЕСКО 2010год) Подготовка учеников к
                                            Олимпиадам Всероссийского и регионального уровня.
                                        </div>
                                        <span class="overflow-message-roll-down js-roll-down g-roll-link">Развернуть</span>
                                        <span class="overflow-message-roll-up js-roll-up g-roll-link">Свернуть</span>
                                    </div>
                                    <div class="slide js-slide">
                                        <div class="slider-image kids">
                                            <img class="g-image" alt="" src="<?= $php_prefix ?>./site/src/images/images-fish/fish-image-2.jpg">
                                        </div>
                                        <div class="name kids">
                                            Страхова Н.В.
                                        </div>
                                        <div class="slider-date">
                                            22.09.2016
                                        </div>
                                        <div class="testimonial js-testimonial">
                                            Для них была организована небольшая экскурсия по школьному музею, где гвоздём программы
                                            была экспозиция с плюшевыми мишками, среди которых находился медвежонок Паддингтон и
                                            Винни Пух. Для гостей пели ученицы Интерлингвы. Девочки в национальных костюмах
                                            Воронежской области исполняли народные песни, миксуя их с известными английскими
                                            шлягерами вроде «Go Down Moses».
                                        </div>
                                        <span class="overflow-message-roll-down js-roll-down g-roll-link">Развернуть</span>
                                        <span class="overflow-message-roll-up js-roll-up g-roll-link">Свернуть</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-12 col-md-12 col-lg-12">
                        <div class="slider-controls">
                            <div class="previous-button kids js-left"></div>
                            <ul class="slider-bulls-container kids js-bulls-container col-sm-offset-3 col-md-offset-3 col-lg-offset-3"></ul>
                            <div class="slider-next-button kids js-right"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="b-description kids">
            <div class="row">
                <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                    <div class="section-title kids">
                        <h2 class="title kids">
                            Описание
                        </h2>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-9 col-md-6 col-lg-6">
                    <p class="g-paragraph">
                        Периферическая профилактическая лазерокоагуляция сетчатки показана <a href="hdfgjdj.com" class="g-kids-link">ссылка
                            в тексте</a> при микроразрывах, возникающих при дистрофии сетчатки и <a href="#" class="g-kids-link">
                            ховер ссылки</a>.
                    </p>
                    <p class="g-paragraph">
                        Чтобы предотвратить отслойку сетчатки, которая плохо поддается лечению, а значит, может стать
                        причиной слепоты, проводится <a href="#" class="g-kids-link">посещенная ссылка</a> обработка лазером
                        истонченных участков.
                    </p>
                    <h3 class="g-third-heading">
                        Суть ППЛК
                    </h3>
                    <p class="g-paragraph">
                        Целью ППЛК является "приваривание" сетчатки, то есть образование в наиболее слабых
                        местах вокруг разрывов спаек с подлежащими тканями.
                    </p>
                    <p class="g-paragraph">
                        Благодаря этому и достигается основная цель процедуры, а именно предупреждение осложнений. Качество же
                        зрения после процедуры во многом зависит от сопутствующих заболеваний.
                    </p>
                    <h3 class="g-third-heading">
                        Преимущества профилактической лазерокоагуляции сетчатки:
                    </h3>
                    <ul class="g-unordered-list">
                        <li class="item">
                            улучшение кровоснабжения;
                        </li>
                        <li class="item">
                            увеличение скорости циркуляции крови;
                        </li>
                        <li class="item">
                            улучшение питания пораженной области;
                        </li>
                    </ul>
                    <h3 class="g-third-heading">
                        Как проходит ППЛК в нашем центре?
                    </h3>
                    <ol class="g-ordered-list">
                        <li class="item">
                            Процедура проводится за один сеанс. Она не требует длительной подготовки и реабилитации.
                        </li>
                        <li class="item">
                            Применяется легкая капельная анестезия.
                        </li>
                    </ol>
                    <p class="g-paragraph">
                        Уже в день операции вы сможете вернуться к привычному образу жизни.
                    </p>
                    <blockquote class="g-blockquote kids">
                        Вам поставлен диагноз "дистрофия сетчатки"? Возьмите ситуацию под контроль: опыт наших специалистов
                        и современное оборудование клиники помогут предупредить отслойку сетчатки и потерю зрения.
                    </blockquote>
                </div>
            </div>
            <div class="row">
                <div class="enroll-form js-form">
                    <div class="section-title">
                        <h2 class="title">
                            Записаться на&#160;прием
                        </h2>
                    </div>
                    <form class="f-form kids">
                        <div class="f-base-message"></div>
                        <div class="f-group">
                            <div class="col-xs-4 col-sm-3 col-sm-offset-3 col-md-2 col-md-offset-1 col-lg-2 col-lg-offset-1">
                                <div class="f-field">
                                    <span class="f-message"></span>
                                    <input class="f-input" type="text" name="name" required>
                                    <label class="f-label" for="name">Ваше имя</label>
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-3 col-md-2 col-lg-2">
                                <div class="f-field">
                                    <span class="f-message"></span>
                                    <input class="f-input js-phone-input" type="tel" name="phone" required>
                                    <label class="f-label" for="phone">Телефон</label>
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-3 col-sm-offset-3 col-md-2 col-lg-2 email-input">
                                <div class="f-field">
                                    <span class="f-message"></span>
                                    <input class="f-input" type="email" name="email" required>
                                    <label class="f-label" for="email">Email</label>
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-3 col-md-2 col-lg-2">
                                <div class="f-field">
                                    <div class="f-select-wrapper js-select">
                                        <select name="service" class="f-select">
                                            <option value="hide">
                                                Тема приема
                                            </option>
                                            <option value="1">
                                                Операции
                                            </option>
                                            <option value="2">
                                                Общие вопросы
                                            </option>
                                            <option value="3">
                                                Диагностика
                                            </option>
                                        </select>
                                        <div class="arrow"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-sm-offset-4 col-md-2 col-lg-2">
                                <div class="f-field">
                                    <input class="f-submit" type="submit" value="Отправить заявку">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="b-stuff kids">
            <div class="row">
                <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                    <div class="section-title kids">
                        <h2 class="title">Услугу оказывают</h2>
                    </div>
                </div>
                <div class="b-slider js-slider">
                    <div class="col-xs-4 col-sm-hide col-md-hide col-lg-hide">
                        <div class="slider-viewport js-viewport">
                            <div class="slides-container js-slides">
                                <div class="slides-wrapper js-slides-wrapper">
                                    <div class="slide js-slide">
                                        <div class="slider-image stuff-photo">
                                            <img class="g-image" alt="" src="<?= $php_prefix ?>./site/src/images/images-fish/stuff-1.jpg">
                                        </div>
                                        <div>
                                            <div class="stuff-second-name g-dotted-link">
                                                Чуриков
                                            </div>
                                        </div>
                                        <div class="stuff-first-name g-dotted-link">
                                            Виктор Николаевич
                                        </div>
                                        <div class="stuff-position">
                                            Главный врач
                                        </div>
                                    </div>
                                    <div class="slide js-slide">
                                        <div class="slider-image stuff-photo">
                                            <img class="g-image" alt="" src="<?= $php_prefix ?>./site/src/images/images-fish/stuff-2.jpg">
                                        </div>
                                        <div>
                                            <div class="stuff-second-name g-dotted-link">
                                                Чуриков
                                            </div>
                                        </div>
                                        <div class="stuff-first-name g-dotted-link">
                                            Виктор Николаевич
                                        </div>
                                        <div class="stuff-position">
                                            Главный врач
                                        </div>
                                    </div>
                                    <div class="slide js-slide">
                                        <div class="slider-image stuff-photo">
                                            <img class="g-image" alt="" src="<?= $php_prefix ?>./site/src/images/images-fish/stuff-3.jpg">
                                        </div>
                                        <div>
                                            <div class="stuff-second-name g-dotted-link">
                                                Чуриков
                                            </div>
                                        </div>
                                        <div class="stuff-first-name g-dotted-link">
                                            Виктор Николаевич
                                        </div>
                                        <div class="stuff-position">
                                            Главный врач
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="slider-controls">
                            <div class="previous-button js-left"></div>
                            <ul class="slider-bulls-container js-bulls-container"></ul>
                            <div class="slider-next-button js-right"></div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-hide">
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <div class="person">
                            <div class="stuff-photo">
                                <img class="g-image" alt="" src="<?= $php_prefix ?>./site/src/images/images-fish/stuff-1.jpg">
                            </div>
                            <div>
                                <div class="stuff-second-name g-dotted-link">
                                    Чуриков
                                </div>
                            </div>
                            <div class="stuff-first-name g-dotted-link">
                                Виктор Николаевич
                            </div>
                            <div class="stuff-position">
                                Главный врач
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <div class="person">
                            <div class="stuff-photo">
                                <img class="g-image" alt="" src="<?= $php_prefix ?>./site/src/images/images-fish/stuff-2.jpg">
                            </div>
                            <div>
                                <div class="stuff-second-name g-dotted-link">
                                    Шишлянникова
                                </div>
                            </div>
                            <div class="stuff-first-name g-dotted-link">
                                Екатерина Владимировна
                            </div>
                            <div class="stuff-position">
                                Врач-офтальмолог
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <div class="person">
                            <div class="stuff-photo">
                                <img class="g-image" alt="" src="<?= $php_prefix ?>./site/src/images/images-fish/stuff-3.jpg">
                            </div>
                            <div>
                                <div class="stuff-second-name g-dotted-link">
                                    Богатырева
                                </div>
                            </div>
                            <div class="stuff-first-name g-dotted-link">
                                Екатерина Сергеевна
                            </div>
                            <div class="stuff-position">
                                Врач-офтальмолог
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-sm-offset-3 col-md-3 col-md-offset-3 col-lg-3 col-lg-offset-3">
                        <div class="person">
                            <div class="stuff-photo">
                                <img class="g-image" alt="" src="<?= $php_prefix ?>./site/src/images/images-fish/stuff-2.jpg">
                            </div>
                            <div>
                                <div class="stuff-second-name g-dotted-link">
                                    Чуриков
                                </div>
                            </div>
                            <div class="stuff-first-name g-dotted-link">
                                Виктор Николаевич
                            </div>
                            <div class="stuff-position">
                                Главный врач
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <div class="person">
                            <div class="stuff-photo">
                                <img class="g-image" alt="" src="<?= $php_prefix ?>./site/src/images/images-fish/stuff-1.jpg">
                            </div>
                            <div>
                                <div class="stuff-second-name g-dotted-link">
                                    Чуриков
                                </div>
                            </div>
                            <div class="stuff-first-name g-dotted-link">Виктор Николаевич</div>
                            <div class="stuff-position">Главный врач</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="b-other-services kids">
            <div class="row">
                <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                    <div class="section-title kids">
                        <h2 class="title kids">
                            Другие услуги
                        </h2>
                        <a href="#" class="commonpage-link">
                            Все услуги клиники
                        </a>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-9 col-md-9 col-lg-9">
                    <div class="services">
                        <div class="service kids">
                            <div class="title">
                                <a href="#" class="link">
                                    Лазерная операция СЛК
                                </a>
                            </div>
                            <div class="description">
                                Секторальная лазеркоагуляция сетчатки.<br>
                                Применяется при тромбозах вен сетчатки и других сосудистых нарушениях.
                            </div>
                            <div class="price">7 000</div>
                        </div>
                        <div class="service kids">
                            <div class="title">
                                <a href="#" class="link">
                                    Дополнительная лазеркоагуляция сетчатки
                                </a>
                            </div>
                            <div class="description">
                                После эндо-лазерной лазеркоагуляции во время витрэктомии.
                            </div>
                            <div class="price">2 500</div>
                        </div>
                        <div class="service kids">
                            <div class="title">
                                <a href="#" class="link">
                                    Лазерная операция СЛК
                                </a>
                            </div>
                            <div class="description">
                                Секторальная лазеркоагуляция сетчатки.<br>
                                Применяется при тромбозах вен сетчатки и других сосудистых нарушениях.
                            </div>
                            <div class="price">7 000</div>
                        </div>
                        <div class="service kids">
                            <div class="title">
                                <a href="#" class="link">
                                    Дополнительная лазерокоагуляция сетчатки
                                </a>
                            </div>
                            <div class="description">
                                После эндо-лазерной лазеркоагуляции во время витрэктомии.
                            </div>
                            <div class="price">2 500</div>
                        </div>
                    </div>
                    <div class="other-services-all-list">
                        <a href="#" class="link">
                            Показать все услуги лазеркоагуляции сетчатки
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>