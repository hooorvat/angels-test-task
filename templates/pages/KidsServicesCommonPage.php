<? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/header-kids.php' ?>
<div class="p-content">
    <div class="container">
        <a name="kids-services-content"></a>
        <div class="b-kids-services">
            <div class="approach">
                <div class="row">
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <div class="section-title kids">
                            <h2 class="title">
                                Подход
                            </h2>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-9 col-md-9 col-lg-9">
                        <div class="experience-container">
                            <div class="row">
                                <div class="col-sm-4 col-md-4 col-lg-4">
                                    <div class="icon-graphic">
                                        <div class="icon-wrapper">
                                            <img class="first-icon" alt="" src="<?= $php_prefix ?>./site/src/images/images-static/icon/kids-services-approach.png">
                                            <img class="icon-background" alt="" src="<?= $php_prefix ?>./site/src/images/images-static/kids-services-background-1.png">
                                        </div>
                                        <div class="approach-description">
                                            Аппаратная диагностика в игровой форме с 3 месяцев
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-md-4 col-lg-4">
                                    <div class="icon-graphic">
                                        <div class="icon-wrapper">
                                            <img class="second-icon" alt="" src="<?= $php_prefix ?>./site/src/images/images-static/icon/kids-services-approach-2.png">
                                            <img class="icon-background" alt="" src="<?= $php_prefix ?>./site/src/images/images-static/kids-services-background-2.png">
                                        </div>
                                        <div class="approach-description">
                                            Огромный объем курсов лечения на современном оборудовании
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-md-4 col-lg-4">
                                    <div class="icon-graphic">
                                        <div class="icon-wrapper">
                                            <img class="third-icon" alt="" src="<?= $php_prefix ?>./site/src/images/images-static/icon/kids-services-approach-3-1.png">
                                            <img class="third-icon cross" alt="" src="<?= $php_prefix ?>./site/src/images/images-static/icon/kids-services-approach-3-2.png">
                                            <img class="icon-background" alt="" src="<?= $php_prefix ?>./site/src/images/images-static/kids-services-background-3.png">
                                        </div>
                                        <div class="approach-description">
                                            Восстановление зрения без операции
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="details">
                <div class="row">
                    <div class="col-xs-hide">
                        <img class="details-first-image" alt="" src="<?= $php_prefix ?>./site/src/images/images-static/kids-services-image-1.png">
                        <img class="details-second-image" alt="" src="<?= $php_prefix ?>./site/src/images/images-static/kids-services-image-2.png">
                        <img class="details-third-image" alt="" src="<?= $php_prefix ?>./site/src/images/images-static/kids-services-image-3.png">
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <div class="section-title kids">
                            <h2 class="title">
                                Главное — детали
                            </h2>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-6 col-md-6 col-lg-6">
                        <p class="g-paragraph">
                            Большое внимание в нашей клинике уделяется лечению детей с миопией, косоглазием и амблиопией.
                            Клиника оснащена уникальным оборудованием, внедрены современные технологии по реабилитации
                            детей с глазной патологией, которые применяются в ведущих клиниках страны.
                        </p>
                        <h3 class="g-third-heading">
                            Комфортные условия
                        </h3>
                        <ul class="g-unordered-list">
                            <li class="item">
                                Высококвалифицированные офтальмологи с многолетним стажем, специализирующиеся на «детских» проблемах.
                            </li>
                            <li class="item">
                                Диагностика и лечение в игровой форме: не страшно, не больно, интересно.
                            </li>
                            <li class="item">
                                Яркий, позитивный дизайн игровая зона
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="services">
                <div class="row">
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <div class="section-title">
                            <h2 class="title">
                                Услуги
                            </h2>
                            <a href="ServicesCommonPage?test" class="commonpage-link">
                                Все услуги клиники
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-9 col-md-9 col-lg-9">
                        <div class="services-section">
                            <h3 class="g-third-heading kids services-section-title">
                                Диагностика
                            </h3>
                            <ul class="services-container">
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Авторефкератометрия
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Бесконтактная биометрия
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Бесконтактная кератопахиметрия
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Бесконтактная пневмотонометрия
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Гониоскопия
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Компьютерная периметрия
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Оптическая когеррентная томография ДЗН
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Оптическая когеррентная томография сетчатки
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Осмотр глазного дна с линзой Гольдмана
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Периметрия
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        УЗ А-сканирование
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        УЗ В-сканирование
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="services-section">
                            <h3 class="g-third-heading kids services-section-title">
                                Лечение
                            </h3>
                            <ul class="services-container">
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Видеокомпьютерная коррекция зрения «Амблиокор»
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Диплоптическое лечение
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Диплоптическое лечение «Форбис»
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Засветы по Кащенко
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Компьютерная диплоптика
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Компьютерная ортоптика
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Компьютерная плеоптика
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Лазерплеоптика «Рубин»
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Лазерстимуляция «Макдэл»
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Лечебно-тренировочная программа
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Лечение "Визотроник"
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Лечение на синоптофоре
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Магнитофорез
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Макулостимуляция
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Оптические тренировки аккомодации вблизи "Форбис"
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Оптические тренировки по Аветисову-Мац
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Оптические тренировки с линзами
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Световая тренировка аккомодации "Каскад"
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Светомагнитотерапия "Амо-Атос"
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Тренировка аккомодации вблизи "Ручеек"
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Цветоимпульсная терапия "Цветоритм"
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Электропунктура "МнДэп"
                                    </a>
                                </li>
                                <li class="service-wrapper">
                                    <a href="KidsServicePage?test" class="service">
                                        Электростимуляция "Эсом-Комет"
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="enroll-form js-form">
                        <div class="section-title">
                            <h2 class="title">
                                Записаться на&#160;прием
                            </h2>
                        </div>
                        <form class="f-form kids">
                            <div class="f-base-message"></div>
                            <div class="f-group">
                                <div class="col-xs-4 col-sm-3 col-sm-offset-3 col-md-2 col-md-offset-1 col-lg-2 col-lg-offset-1">
                                    <div class="f-field">
                                        <span class="f-message"></span>
                                        <input class="f-input" type="text" name="name" required>
                                        <label class="f-label" for="name">Ваше имя</label>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-3 col-md-2 col-lg-2">
                                    <div class="f-field">
                                        <span class="f-message"></span>
                                        <input class="f-input js-phone-input" type="tel" name="phone" required>
                                        <label class="f-label" for="phone">Телефон</label>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-3 col-sm-offset-3 col-md-2 col-lg-2 email-input">
                                    <div class="f-field">
                                        <span class="f-message"></span>
                                        <input class="f-input" type="email" name="email" required>
                                        <label class="f-label" for="email">Email</label>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-3 col-md-2 col-lg-2">
                                    <div class="f-field">
                                        <div class="f-select-wrapper js-select">
                                            <select name="service" class="f-select">
                                                <option value="hide">
                                                    Тема приема
                                                </option>
                                                <option value="1">
                                                    Операции
                                                </option>
                                                <option value="2">
                                                    Общие вопросы
                                                </option>
                                                <option value="3">
                                                    Диагностика
                                                </option>
                                            </select>
                                            <div class="arrow"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-sm-offset-4 col-md-2 col-lg-2">
                                    <div class="f-field">
                                        <input class="f-submit" type="submit" value="Отправить заявку">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>