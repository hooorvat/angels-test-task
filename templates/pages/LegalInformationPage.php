<? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/header-white.php' ?>
<div class="p-content">
    <div class="container">
        <div class="b-supervisory-authority">
            <div class="row">
                <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                    <div class="section-title">
                        <h2 class="title">
                            Контролирующие органы
                        </h2>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-9 col-md-9 col-lg-9">
                    <div class="supervisory-authority-container">
                        <div class="row">
                            <div class="col-xs-4 col-sm-8 col-md-8 col-lg-8">
                                <div class="title-wrapper">
                                    <a href="http://www.oblzdrav.vrn.ru/" target="_blank" class="g-title-link">
                                        Департамент здравоохранения Воронежской области
                                    </a>
                                </div>
                                <h4 class="heading">
                                    Адрес
                                </h4>
                                <p class="content">
                                    394036, г. Воронеж, ул. Никитинская, д. 5
                                </p>
                                <h4 class="heading">
                                    Телефоны
                                </h4>
                                <p class="content">
                                    +7 (473) 253-10-51; +7 (473) 252-02-26 — факс
                                </p>
                                <h4 class="heading">
                                    Официальный сайт:
                                </h4>
                                <a href="http://www.oblzdrav.vrn.ru/" target="_blank" class="content site">
                                    http://www.oblzdrav.vrn.ru/
                                </a>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <img class="blazon" src="<?= $php_prefix ?>./site/src/images/images-fish/blazon-1.png">
                            </div>
                        </div>
                    </div>
                    <div class="supervisory-authority-container">
                        <div class="row">
                            <div class="col-xs-4 col-sm-8 col-md-8 col-lg-8">
                                <div class="title-wrapper">
                                    <a href="http://36reg.roszdravnadzor.ru/" target="_blank" class="g-title-link">
                                        Территориальный орган Росздравнадзора по Воронежской области
                                    </a>
                                </div>
                                <h4 class="heading">
                                    Адрес
                                </h4>
                                <p class="content">
                                    394018, г. Воронеж, ул. 9-го января, д.36, 1 этаж
                                </p>
                                <h4 class="heading">
                                    Телефоны
                                </h4>
                                <p class="content">
                                    +7 (473) 276-53-99
                                </p>
                                <h4 class="heading">
                                    Официальный сайт:
                                </h4>
                                <a href="http://36reg.roszdravnadzor.ru/" target="_blank" class="content site">
                                    http://36reg.roszdravnadzor.ru/
                                </a>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <img class="blazon" src="<?= $php_prefix ?>./site/src/images/images-fish/blazon-2.png">
                            </div>
                        </div>
                    </div>
                    <div class="supervisory-authority-container">
                        <div class="row">
                            <div class="col-xs-4 col-sm-8 col-md-8 col-lg-8">
                                <div class="title-wrapper">
                                    <a href="http://www.36.rospotrebnadzor.ru/" target="_blank" class="g-title-link">
                                        Управление федеральной службы по надзору в сфере защиты прав потребителей и благополучия человека по Воронежской области
                                    </a>
                                </div>
                                <h4 class="heading">
                                    Адрес
                                </h4>
                                <p class="content">
                                    394038, г. Воронеж, ул. Космонавтов, 21а
                                </p>
                                <h4 class="heading">
                                    Телефоны
                                </h4>
                                <p class="content">
                                    +7 (473) 263-77-27; +7 (473) 264-14-77 — факс
                                </p>
                                <h4 class="heading">
                                    Официальный сайт:
                                </h4>
                                <a href="http://www.36.rospotrebnadzor.ru/" target="_blank" class="content site">
                                    http://www.36.rospotrebnadzor.ru/
                                </a>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <img class="blazon" src="<?= $php_prefix ?>./site/src/images/images-fish/blazon-3.jpg">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="b-documents">
            <div class="row">
                <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                    <div class="section-title">
                        <h2 class="title">
                            Документы
                        </h2>
                    </div>
                </div>
                <div class="col-xs-hide">
                    <div class="b-slider js-slider">
                        <div class="col-sm-9 col-md-9 col-lg-9">
                            <div class="slider-viewport js-viewport">
                                <div class="slides-container js-slides">
                                    <div class="slides-wrapper js-slides-wrapper">
                                        <div class="slide js-slide">
                                            <div class="row">
                                                <div class="col-sm-4 col-md-4 col-lg-4">
                                                    <div class="document-image-wrapper">
                                                        <img class="document-image" src="<?= $php_prefix ?>./site/src/images/images-fish/document-1.png">
                                                        <div class="substrate first"></div>
                                                        <div class="substrate second"></div>
                                                    </div>
                                                    <a class="document-title js-popup-open" data-target="documentsPopup">
                                                        Свидетельство о регистрации юридического лица
                                                    </a>
                                                </div>
                                                <div class="col-sm-4 col-md-4 col-lg-4">
                                                    <div class="document-image-wrapper">
                                                        <img class="document-image" src="<?= $php_prefix ?>./site/src/images/images-fish/document-2.png">
                                                        <div class="substrate first"></div>
                                                        <div class="substrate second"></div>
                                                    </div>
                                                    <a href="#" class="document-title js-popup-open" data-target="documentsPopup">
                                                        Свидетельство о постановке на учет российской организации в налоговом органе
                                                    </a>
                                                </div>
                                                <div class="col-sm-4 col-md-4 col-lg-4">
                                                    <div class="document-image-wrapper multi">
                                                        <img class="document-image" src="<?= $php_prefix ?>./site/src/images/images-fish/document-3.png">
                                                        <div class="substrate first"></div>
                                                        <div class="substrate second"></div>
                                                    </div>
                                                    <a class="document-title js-popup-open" data-target="documentsPopup">
                                                        Лицензии на осуществление медицинской деятельности
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slide js-slide">
                                            <div class="row">
                                                <div class="col-sm-4 col-md-4 col-lg-4">
                                                    <div class="document-image-wrapper">
                                                        <img class="document-image" src="<?= $php_prefix ?>./site/src/images/images-fish/document-1.png">
                                                    </div>
                                                    <a class="document-title js-popup-open" data-target="documentsPopup">
                                                        Свидетельство о регистрации юридического лица
                                                    </a>
                                                </div>
                                                <div class="col-sm-4 col-md-4 col-lg-4">
                                                    <div class="document-image-wrapper">
                                                        <img class="document-image" src="<?= $php_prefix ?>./site/src/images/images-fish/document-2.png">
                                                    </div>
                                                    <a href="#" class="document-title js-popup-open" data-target="documentsPopup">
                                                        Свидетельство о постановке на учет российской организации в налоговом органе
                                                    </a>
                                                </div>
                                                <div class="col-sm-4 col-md-4 col-lg-4">
                                                    <div class="document-image-wrapper multi">
                                                        <img class="document-image" src="<?= $php_prefix ?>./site/src/images/images-fish/document-3.png">
                                                        <div class="substrate first"></div>
                                                        <div class="substrate second"></div>
                                                    </div>
                                                    <a class="document-title js-popup-open" data-target="documentsPopup">
                                                        Лицензии на осуществление медицинской деятельности
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slide js-slide">
                                            <div class="row">
                                                <div class="col-sm-4 col-md-4 col-lg-4">
                                                    <div class="document-image-wrapper">
                                                        <img class="document-image" src="<?= $php_prefix ?>./site/src/images/images-fish/document-1.png">
                                                    </div>
                                                    <a class="document-title js-popup-open" data-target="documentsPopup">
                                                        Свидетельство о регистрации юридического лица
                                                    </a>
                                                </div>
                                                <div class="col-sm-4 col-md-4 col-lg-4">
                                                    <div class="document-image-wrapper">
                                                        <img class="document-image" src="<?= $php_prefix ?>./site/src/images/images-fish/document-2.png">
                                                    </div>
                                                    <a href="#" class="document-title js-popup-open" data-target="documentsPopup">
                                                        Свидетельство о постановке на учет российской организации в налоговом органе
                                                    </a>
                                                </div>
                                                <div class="col-sm-4 col-md-4 col-lg-4">
                                                    <div class="document-image-wrapper multi">
                                                        <img class="document-image" src="<?= $php_prefix ?>./site/src/images/images-fish/document-3.png">
                                                        <div class="substrate first"></div>
                                                        <div class="substrate second"></div>
                                                    </div>
                                                    <a class="document-title js-popup-open" data-target="documentsPopup">
                                                        Лицензии на осуществление медицинской деятельности
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-4 col-sm-12 col-md-12 col-lg-12">
                            <div class="slider-controls">
                                <div class="previous-button js-left"></div>
                                <ul class="slider-bulls-container js-bulls-container col-sm-offset-3 col-md-offset-3 col-lg-offset-3"></ul>
                                <div class="slider-next-button js-right"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-hide col-md-hide col-lg-hide">
                    <div class="b-slider js-slider">
                        <div class="slider-viewport js-viewport">
                            <div class="slides-container js-slides">
                                <div class="slides-wrapper js-slides-wrapper">
                                    <div class="slide js-slide">
                                        <div class="document-image-wrapper">
                                            <img class="document-image" src="<?= $php_prefix ?>./site/src/images/images-fish/document-1.png">
                                            <div class="substrate first"></div>
                                            <div class="substrate second"></div>
                                        </div>
                                        <a class="document-title js-popup-open" data-target="documentsPopup">
                                            Свидетельство о регистрации юридического лица
                                        </a>
                                    </div>
                                    <div class="slide js-slide">
                                        <div class="document-image-wrapper">
                                            <img class="document-image" src="<?= $php_prefix ?>./site/src/images/images-fish/document-1.png">
                                        </div>
                                        <a class="document-title js-popup-open" data-target="documentsPopup">
                                            Свидетельство о регистрации юридического лица
                                        </a>
                                    </div>
                                    <div class="slide js-slide">
                                        <div class="document-image-wrapper">
                                            <img class="document-image" src="<?= $php_prefix ?>./site/src/images/images-fish/document-1.png">
                                        </div>
                                        <a class="document-title js-popup-open" data-target="documentsPopup">
                                            Свидетельство о регистрации юридического лица
                                        </a>
                                    </div>
                                    <div class="slide js-slide">
                                        <div class="document-image-wrapper">
                                            <img class="document-image" src="<?= $php_prefix ?>./site/src/images/images-fish/document-1.png">
                                        </div>
                                        <a class="document-title js-popup-open" data-target="documentsPopup">
                                            Свидетельство о регистрации юридического лица
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-4 col-sm-12 col-md-12 col-lg-12">
                            <div class="slider-controls">
                                <div class="previous-button js-left"></div>
                                <ul class="slider-bulls-container js-bulls-container col-sm-offset-3 col-md-offset-3 col-lg-offset-3"></ul>
                                <div class="slider-next-button js-right"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/popups/documents-popup.php' ?>

