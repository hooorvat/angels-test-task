<? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/header-main.php' ?>
<div class="p-content">
    <div class="container">
        <a name="main-page-content"></a>
        <div class="b-main-page-services">
            <div class="row">
                <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                    <div class="section-title">
                        <h2 class="title">
                            Услуги
                        </h2>
                        <a href="ServicesCommonPage?test" class="commonpage-link">
                            Перейти в раздел «Услуги»
                        </a>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-9 col-md-9 col-lg-9">
                    <div class="services-container js-accordion">
                        <div class="row">
                            <div class="col-sm-4 col-md-4 col-lg-4">
                                <div class="service-category js-category">
                                    <a href="ServicesCommonPage?test#diagnostic" class="category-title">
                                        Диагностика
                                    </a>
                                    <ul class="services-list js-services-list">
                                        <li class="service-wrapper">
                                            <a href="ServicePage?test" class="service">
                                                УЗИ глаза методом А-сканирования
                                            </a>
                                        </li>
                                        <li class="service-wrapper">
                                            <a href="ServicePage?test" class="service">
                                                УЗИ глаза методом В-сканирования
                                            </a>
                                        </li>
                                        <li class="service-wrapper">
                                            <a href="./ServicePage?test" class="service">
                                                Визометрия
                                            </a>
                                        </li>
                                        <li class="service-wrapper">
                                            <a href="ServicePage?test" class="service">
                                                Компьютерная периметрия
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4 col-lg-4">
                                <div class="service-category js-category">
                                    <a href="ServicesCommonPage?test#cataract-cure" class="category-title">
                                        Лечение катаракты
                                    </a>
                                    <ul class="services-list js-services-list">
                                        <li class="service-wrapper">
                                            <a href="ServicePage?test" class="service">
                                                Факоэмульсификация катаракты <br> 1 категории сложности без имплантации ИОЛ
                                            </a>
                                        </li>
                                        <li class="service-wrapper">
                                            <a href="ServicePage?test" class="service">
                                                Факоэмульсификация катаракты 2 категории сложности <br> без имплантации ИОЛ
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4 col-lg-4">
                                <div class="service-category js-category">
                                    <a href="ServicesCommonPage?test#glaucoma-cure" class="category-title">
                                        Лечение глаукомы
                                    </a>
                                    <ul class="services-list js-services-list">
                                        <li class="service-wrapper">
                                            <a href="ServicePage?test" class="service">
                                                Непроникающая глубокая склерэктомия <br> (НГСЭ)
                                            </a>
                                        </li>
                                        <li class="service-wrapper">
                                            <a href="ServicePage?test" class="service">
                                                Синустрабекулоэктомия с задней склерэктомией <br> (СТЭ+ЗСЭ)
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 col-md-4 col-lg-4">
                                <div class="service-category js-category">
                                    <a href="ServicesCommonPage?test#retina-cure" class="category-title">
                                        Лазеркоагуляция сетчатки
                                    </a>
                                    <ul class="services-list js-services-list">
                                        <li class="service-wrapper">
                                            <a href="ServicePage?test" class="service">
                                                Лазерная десцеметогониопунтура
                                            </a>
                                        </li>
                                        <li class="service-wrapper">
                                            <a href="ServicePage?test" class="service">
                                                Лазерная десцеметогониопунтура (второй этап после НГСЭ в ООО "ЦКО "Мединвест")
                                            </a>
                                        </li>
                                        <li class="service-wrapper">
                                            <a href="ServicePage?test" class="service">
                                                Лазерная операция ОЛК
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4 col-lg-4">
                                <div class="service-category js-category">
                                    <a href="ServicesCommonPage?test#restoring-cure" class="category-title">
                                        Восстановительное лечение
                                    </a>
                                    <ul class="services-list js-services-list">
                                        <li class="service-wrapper">
                                            <a href="ServicePage?test" class="service">
                                                Оптические тренировки с линзами
                                            </a>
                                        </li>
                                        <li class="service-wrapper">
                                            <a href="ServicePage?test" class="service">
                                                Оптические тренировки <br> по Аветисову-Мац
                                            </a>
                                        </li>
                                        <li class="service-wrapper">
                                            <a href="ServicePage?test" class="service">
                                                Оптические тренировки аккомодации вблизи ("Форбис")
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4 col-lg-4">
                                <div class="service-category js-category">
                                    <a href="ServicesCommonPage?test#other-services-and-operations" class="category-title">
                                        Прочие услуги и операции
                                    </a>
                                    <ul class="services-list js-services-list">
                                        <li class="service-wrapper">
                                            <a href="ServicePage?test" class="service">
                                                Удаление инородных тел роговицы
                                            </a>
                                        </li>
                                        <li class="service-wrapper">
                                            <a href="ServicePage?test" class="service">
                                                Лечение новообразований
                                            </a>
                                        </li>
                                        <li class="service-wrapper">
                                            <a href="ServicePage?test" class="service">
                                                Удаление птеригиума
                                            </a>
                                        </li>
                                        <li class="service-wrapper">
                                            <a href="ServicePage?test" class="service">
                                                Лечение халязиона
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-hide">
            <div class="b-main-page-discounts">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="title">
                            Скидки и акции
                        </div>
                        <div class="np-col-sm-10 col-sm-offset-1 np-col-md-10 col-md-offset-1 np-col-lg-10 col-lg-offset-1">
                            <div class="discounts-container">
                                <div class="row">
                                    <div class="col-xs-4 col-sm-6 col-md-6 col-lg-6">
                                        <a href="#" class="discount">
                                            <div class="discount-title-wrapper">
                                                <div class="discount-title">
                                                    Скидка до 10%
                                                </div>
                                            </div>
                                            <div class="discount-description">
                                                Скидка участникам фестиваля <br> в парке «Алые паруса»!
                                            </div>
                                            <div class="discount-date">
                                                До 31 декабря 2016
                                            </div>
                                            <div class="discount-icon">
<!--                                                <img alt="" src="--><?//= $php_prefix ?><!--./site/src/images/images-static/icon/percent.svg">-->
                                                <img alt="" src="<?= $php_prefix ?>./site/src/images/images-static/icon/family.svg">
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-xs-4 col-sm-6 col-md-6 col-lg-6">
                                        <a href="#" class="discount">
                                            <div class="discount-title-wrapper">
                                                <div class="discount-title">
                                                    Акция для ветеранов ВОВ
                                                </div>
                                            </div>
                                            <div class="discount-description">
                                                Ветеранам ВОВ: первичный прием - бесплатно!
                                            </div>
                                            <div class="discount-date">
                                                Без срока
                                            </div>
                                            <div class="discount-icon">
<!--                                                <img alt="" src="--><?//= $php_prefix ?><!--./site/src/images/images-static/icon/star.svg">-->
                                                <img alt="" src="<?= $php_prefix ?>./site/src/images/images-static/icon/boy.svg">
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-4 col-sm-hide col-md-hide col-lg-hide">
            <div class="b-main-page-discounts">
                <div class="row">
                    <div class="section-title">
                        <h2 class="title">
                            Скидки и акции
                        </h2>
                    </div>
                    <div class="b-slider js-slider">
                        <div class="slider-viewport js-viewport">
                            <div class="slides-container js-slides">
                                <div class="slides-wrapper js-slides-wrapper">
                                    <div class="slide js-slide">
                                        <a href="#" class="discount">
                                            <div class="discount-title-wrapper">
                                                <div class="discount-title">
                                                    Скидка до 10%
                                                </div>
                                            </div>
                                            <div class="discount-description">
                                                Скидка участникам фестиваля <br> в парке «Алые паруса»!
                                            </div>
                                            <div class="discount-date">
                                                До 31 декабря 2016
                                            </div>
                                            <div class="discount-icon">
                                                <img alt="" src="<?= $php_prefix ?>./site/src/images/images-static/icon/percent.svg">
                                            </div>
                                        </a>
                                    </div>
                                    <div class="slide js-slide">
                                        <a href="#" class="discount">
                                            <div class="discount-title-wrapper">
                                                <div class="discount-title">
                                                    Акция для ветеранов ВОВ
                                                </div>
                                            </div>
                                            <div class="discount-description">
                                                Ветеранам ВОВ: первичный прием - бесплатно!
                                            </div>
                                            <div class="discount-date">
                                                Без срока
                                            </div>
                                            <div class="discount-icon">
                                                <img alt="" src="<?= $php_prefix ?>./site/src/images/images-static/icon/star.svg">
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="slider-controls">
                            <div class="previous-button js-left"></div>
                            <ul class="slider-bulls-container js-bulls-container col-sm-offset-3 col-md-offset-3 col-lg-offset-3"></ul>
                            <div class="slider-next-button js-right"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="b-testimonials">
            <div class="row">
                <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                    <div class="section-title">
                        <h2 class="title">
                            Отзывы
                        </h2>
                        <a href="#" class="commonpage-link">
                            Посмотреть все отзывы
                        </a>
                    </div>
                </div>
                <div class="b-slider js-slider">
                    <div class="col-xs-4 col-sm-9 col-md-9 col-lg-9">
                        <div class="slider-viewport js-viewport">
                            <div class="slides-container js-slides">
                                <div class="slides-wrapper js-slides-wrapper">
                                    <div class="slide js-slide">
                                        <div class="slider-image">
                                            <img class="g-image" alt="" src="<?= $php_prefix ?>./site/src/images/images-fish/slider-image.jpg">
                                        </div>
                                        <div class="name">
                                            Страхова Н.В.
                                        </div>
                                        <div class="slider-date">
                                            22.09.2016
                                        </div>
                                        <div class="testimonial js-testimonial">
                                            Клиника супер!!!! Оборудование, дизайн, специалисты. На все вопросы я получила квалифицированные
                                            ответы (врач Богатырева Е.С.). Врач и остальной персонал доброжелательны. Когда сынок раскапризничался,
                                            врач позвала еще од сотрудника и они терпеливо развлекали моего капризюлю и гремели во все погремушки
                                            разом. Отдельно хочется отметить дизайн - очень клас.
                                            У сына аж рот открылся, глядя на яркий великолепный диван! Да, признаться, и у нас с мужем тоже!
                                        </div>
                                        <span class="overflow-message-roll-down js-roll-down g-roll-link">Развернуть</span>
                                        <span class="overflow-message-roll-up js-roll-up g-roll-link">Свернуть</span>
                                    </div>
                                    <div class="slide js-slide">
                                        <div class="slider-image">
                                            <img class="g-image" alt="" src="<?= $php_prefix ?>./site/src/images/images-fish/stuff-2.jpg">
                                        </div>
                                        <div class="name">
                                            Иванов Н.В.
                                        </div>
                                        <div class="slider-date">
                                            2.29.2014
                                        </div>
                                        <div class="testimonial js-testimonial">
                                            Повышение Квалификации: Семинар по методике преподавания английского языка на разных
                                            этапах обучения (Макмиллан, 11.03.2013) "Совершенствование иноязычного образования в
                                            школе и современные системы тестирования" Цикл семинаров с 29.10.2014 по 25.03.2013
                                            (5 часов) Oxford Professional Development Seminar. Learning the Language and Growing
                                            Professionally 08.04.2013 Занималась подготовкой участника научно-практической
                                            конференции "Мир иностранных языков" 06.04.2013(Лингвострановедение- Байконур.Казахстан)
                                            Подготовка участника конкурса эссэ (проект ЮНЕСКО 2010год) Подготовка учеников к
                                            Олимпиадам Всероссийского и регионального уровня.
                                        </div>
                                        <span class="overflow-message-roll-down js-roll-down g-roll-link">Развернуть</span>
                                        <span class="overflow-message-roll-up js-roll-up g-roll-link">Свернуть</span>
                                    </div>
                                    <div class="slide js-slide">
                                        <div class="slider-image">
                                            <img class="g-image" alt="" src="<?= $php_prefix ?>./site/src/images/images-fish/stuff-1.jpg">
                                        </div>
                                        <div class="name">
                                            Страхова Н.В.
                                        </div>
                                        <div class="slider-date">
                                            22.09.2016
                                        </div>
                                        <div class="testimonial  js-testimonial">
                                            Для них была организована небольшая экскурсия по школьному музею, где гвоздём программы
                                            была экспозиция
                                        </div>
                                        <span class="overflow-message-roll-down js-roll-down g-roll-link">Развернуть</span>
                                        <span class="overflow-message-roll-up js-roll-up g-roll-link">Свернуть</span>
                                    </div>
                                    <div class="slide js-slide">
                                        <div class="slider-image">
                                            <img class="g-image" alt="" src="<?= $php_prefix ?>./site/src/images/images-fish/fish-image-1.jpg">
                                        </div>
                                        <div class="name">
                                            Перов Н.В.
                                        </div>
                                        <div class="slider-date">
                                            27.01.2056
                                        </div>
                                        <div class="testimonial js-testimonial">
                                            Повышение Квалификации: Семинар по методике преподавания английского языка на разных
                                            этапах обучения (Макмиллан, 11.03.2013) "Совершенствование иноязычного образования в
                                            школе и современные системы тестирования" Цикл семинаров с 29.10.2014 по 25.03.2013
                                            (5 часов) Oxford Professional Development Seminar. Learning the Language and Growing
                                            Professionally 08.04.2013 Занималась подготовкой участника научно-практической
                                            конференции "Мир иностранных языков" 06.04.2013(Лингвострановедение- Байконур.Казахстан)
                                            Подготовка участника конкурса эссэ (проект ЮНЕСКО 2010год) Подготовка учеников к
                                            Олимпиадам Всероссийского и регионального уровня.
                                        </div>
                                        <span class="overflow-message-roll-down js-roll-down g-roll-link">Развернуть</span>
                                        <span class="overflow-message-roll-up js-roll-up g-roll-link">Свернуть</span>
                                    </div>
                                    <div class="slide js-slide">
                                        <div class="slider-image">
                                            <img class="g-image" alt="" src="<?= $php_prefix ?>./site/src/images/images-fish/fish-image-2.jpg">
                                        </div>
                                        <div class="name">
                                            Страхова Н.В.
                                        </div>
                                        <div class="slider-date">
                                            22.09.2016
                                        </div>
                                        <div class="testimonial js-testimonial">
                                            Для них была организована небольшая экскурсия по школьному музею, где гвоздём программы
                                            была экспозиция с плюшевыми мишками, среди которых находился медвежонок Паддингтон и
                                            Винни Пух. Для гостей пели ученицы Интерлингвы. Девочки в национальных костюмах
                                            Воронежской области исполняли народные песни, миксуя их с известными английскими
                                            шлягерами вроде «Go Down Moses».
                                        </div>
                                        <span class="overflow-message-roll-down js-roll-down g-roll-link">Развернуть</span>
                                        <span class="overflow-message-roll-up js-roll-up g-roll-link">Свернуть</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-12 col-md-12 col-lg-12">
                        <div class="slider-controls">
                            <div class="previous-button js-left"></div>
                            <ul class="slider-bulls-container js-bulls-container col-sm-offset-3 col-md-offset-3 col-lg-offset-3"></ul>
                            <div class="slider-next-button js-right"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="b-main-page-experience">
            <div class="row">
                <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                    <div class="section-title">
                        <h2 class="title">
                            Опыт
                        </h2>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-9 col-md-9 col-lg-9">
                    <div class="experience-container">
                        <div class="row">
                            <div class="col-sm-4 col-md-4 col-lg-4">
                                <div class="icon-graphic">
                                    <img class="icon" alt="" src="<?= $php_prefix ?>./site/src/images/images-static/icon/graphic-science.svg">
                                    <div class="number-first">
                                        25
                                    </div>
                                    <div class="divider"></div>
                                    <div class="statistic">
                                        25 докторов наук
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4 col-lg-4">
                                <div class="icon-graphic">
                                    <img class="icon" alt="" src="<?= $php_prefix ?>./site/src/images/images-static/icon/graphic-microscope.svg">
                                    <div class="number-second">
                                        175
                                    </div>
                                    <div class="divider"></div>
                                    <div class="statistic">
                                        175 лет практики
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4 col-lg-4">
                                <div class="icon-graphic">
                                    <img class="icon" alt="" src="<?= $php_prefix ?>./site/src/images/images-static/icon/graphic-eye.svg">
                                    <div class="number-third">
                                        300001
                                    </div>
                                    <div class="divider"></div>
                                    <div class="statistic">
                                        300 001 излеченный глаз
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8 col-md-8 col-lg-8">
                                <div class="quote">
                                    <div class="name">
                                        <div class="second-name">
                                            Чуриков
                                        </div><br>
                                        <div class="first-name">
                                            Виктор Николаевич
                                        </div>
                                    </div>
                                    <div class="position">
                                        Главный врач, кандидат медицинских наук, <br>
                                        доцент кафедры Офтальмологии ВГМА им. Н. Н. Бурденко
                                    </div>
                                    <div class="quote-text g-blockquote">
                                        «Человек может быть по-настоящему счастлив только тогда,
                                        когда любит свою специальность, увлечен работой и всей душой предан ей, когда чувствует,
                                        что он необходим обществу и его труд приносит пользу людям.»
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-hide col-sm-4 col-md-4 col-lg-4">
                                <div class="image"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="b-main-page-events">
            <div class="row">
                <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                    <div class="section-title">
                        <h2 class="title">
                            События
                        </h2>
                        <a href="NewsCommonPage?test" class="commonpage-link">
                            Перейти в раздел «Новости»
                        </a>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-9 col-md-9 col-lg-9">
                    <div class="events-container">
                        <div class="event">
                            <a href="ContentPage?test" class="title">
                                Диагностический центр «Здоровье» в Богучаре
                            </a>
                            <div class="description">
                                Сотрудничество в Воронежской области
                            </div>
                            <div class="date">1 октября 2016</div>
                        </div>
                        <div class="event">
                            <a href="ContentPage?test" class="title">
                                Международная офтальмологическая конеференция в Копенгагене
                            </a>
                            <div class="description">
                                ЦКО «МЕДИНВЕСТ» представлял главный врач клиники Чуриков Виктор Николаевич.
                            </div>
                            <div class="date">8 сентября 2016</div>
                        </div>
                        <div class="event">
                            <a href="ContentPage?test" class="title">
                                Диагностический центр «Здоровье» в Богучаре
                            </a>
                            <div class="description">
                                Сотрудничество в Воронежской области
                            </div>
                            <div class="date">1 октября 2016</div>
                        </div>
                        <div class="event">
                            <a href="ContentPage?test" class="title">
                                Международная офтальмологическая конеференция в Копенгагене
                            </a>
                            <div class="description">
                                ЦКО «МЕДИНВЕСТ» представлял главный врач клиники Чуриков Виктор Николаевич.
                            </div>
                            <div class="date">8 сентября 2016</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
