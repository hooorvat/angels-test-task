<? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/header-white.php' ?>
<div class="p-content">
    <div class="container">
        <div class="b-news-common">
            <div class="news-wrapper">
                <div class="row">
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <ul class="news-sort">
                            <li>
                                <a class="news-sort-item active">
                                    Все материалы
                                </a>
                            </li>
                            <li>
                                <a class="news-sort-item">
                                    Новости
                                </a>
                            </li>
                            <li>
                                <a class="news-sort-item">
                                    Статьи
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-xs-4 col-sm-9 col-md-9 col-lg-9">
                        <div class="news-container">
                            <div class="news">
                                <div class="image">
                                    <img class="g-image" alt="" src="<?= $php_prefix ?>./site/src/images/images-fish/fish-image-2.jpg">
                                </div>
                                <a href="#" class="heading">
                                    Международная офтальмологическая конференция в копенгагене
                                </a>
                                <div class="content">
                                    ЦКО "Мединвест" представлял главный врач клиники Чуриков Виктор
                                    Николаевич.
                                </div>
                                <div class="date">
                                    Новость 8 сентября 2016
                                </div>
                            </div>
                            <div class="news">
                                <div class="image">
                                    <img class="g-image" alt="" src="<?= $php_prefix ?>./site/src/images/images-fish/fish-image-2.jpg">
                                </div>
                                <a href="#" class="heading">
                                    Диагностический центр "Здоровье" в Богучаре
                                </a>
                                <div class="content">
                                    Сотрудничество в Воронежской области
                                </div>
                                <div class="date">
                                    Новость 1 октября 2016
                                </div>
                            </div>
                            <div class="news">
                                <div class="image">
                                    <img class="g-image" alt="" src="<?= $php_prefix ?>./site/src/images/images-fish/fish-image-2.jpg">
                                </div>
                                <a href="#" class="heading">
                                    Бесплатный скрининг от ЦКО "Мединвест"
                                </a>
                                <div class="content">
                                    Социальный проект ко Всемирному дню зрения
                                </div>
                                <div class="date">
                                    Новость 2 сентября 2016
                                </div>
                            </div>
                            <div class="news">
                                <div class="image">
                                    <img class="g-image" alt="" src="<?= $php_prefix ?>./site/src/images/images-fish/fish-image-2.jpg">
                                </div>
                                <a href="#" class="heading">
                                    Создание Университетской клиники
                                </a>
                                <div class="content">
                                    Воронежский государственный медицинский университет им. Н.Н. Бурденко организовали
                                </div>
                                <div class="date">
                                    Новость 28 августа 2016
                                </div>
                            </div>
                            <div class="news">
                                <div class="image">
                                    <img class="g-image" alt="" src="<?= $php_prefix ?>./site/src/images/images-fish/fish-image-2.jpg">
                                </div>
                                <a href="#" class="heading">
                                    "Классный День" от ЦКО "Мединвест"
                                </a>
                                <div class="content">
                                    Очередное мероприятие в "Алых парусах"
                                </div>
                                <div class="date">
                                    Новость 9 апреля 2016
                                </div>
                            </div>
                            <div class="news">
                                <div class="image">
                                    <img class="g-image" alt="" src="<?= $php_prefix ?>./site/src/images/images-fish/fish-image-2.jpg">
                                </div>
                                <a href="#" class="heading">
                                    Европейский конгресс хирургов ESCRS 2015
                                </a>
                                <div class="content">
                                    Международный конгресс в участием специалиста нашей клиники.
                                </div>
                                <div class="date">
                                    Новость 1 октября 2016
                                </div>
                            </div>
                            <div class="news">
                                <div class="image">
                                    <img class="g-image" alt="" src="<?= $php_prefix ?>./site/src/images/images-fish/fish-image-2.jpg">
                                </div>
                                <a href="#" class="heading">
                                    Связь между анатомией глаза и риском возникновения глаукомы у разных этносов
                                </a>
                                <div class="content">
                                    Различия в анатомии глаза у разных этносов могут определять степень риска возникновения
                                    глаукомы.
                                </div>
                                <div class="date">
                                    Новость 6 сентября 2015
                                </div>
                            </div>
                            <div class="news">
                                <div class="image">
                                    <img class="g-image" alt="" src="<?= $php_prefix ?>./site/src/images/images-fish/fish-image-2.jpg">
                                </div>
                                <a href="#" class="heading">
                                    Выявлены оптимальные сроки для удаления катаракты
                                </a>
                                <div class="content">
                                    Исследование, опубликованному в январском номере Ophthalmology.
                                </div>
                                <div class="date">
                                    Новость 28 августа 2016
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/blocks/pagination.php' ?>
        </div>
    </div>
</div>