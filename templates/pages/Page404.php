<? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/header-white.php' ?>
<div class="p-content">
    <div class="container">
        <div class="b-errorPage">
            <div class="row">
                <div class="col-xs-4 col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">
                    <p class="g-paragraph">
                        Страница, которую вы запросили, не найдена на нашем сервере. <br>
                        Возможно, эта страница удалена, переименована <br> или просто временно недоступна.
                    </p>
                    <h3 class="g-third-heading">
                        Найти то, что вы ищите, можно:
                    </h3>
                    <ul class="g-unordered-list">
                        <li class="item">в меню сайта;</li>
                        <li class="item">используя <a class="js-error-404-search g-link">поиск</a>;</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>