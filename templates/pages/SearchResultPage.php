<? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/header-white.php' ?>
<div class="p-content">
    <div class="container">
        <div class="b-search-results">
            <div class="row">
                <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                    <div class="section-title">
                        <h2 class="title">
                            По запросу
                        </h2>
                        <div class="description">
                            44 совпадения
                        </div>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-9 col-md-9 col-lg-9">
                    <div class="search-results-container">
                        <div class="search-wrapper">
                            <input class="search-input">
                            <span class="g-icon icon-search"></span>
                        </div>
                        <div class="search-result">
                            <div class="title-wrapper">
                                <a href="NewsPage?test" class="g-title-link">
                                    ЦКО "Медивест" - "Эксперт в офтальмологии 2016"!
                                </a>
                            </div>
                            <p class="description g-paragraph">
                                Мы рады сообщить, что экспертом в области офльмологии признан Центр Клинической Офтальмологии "Мединвест"!
                            </p>
                            <a href="http://oftalmolog36.ru/news/678/" class="link">
                                http://oftalmolog36.ru/news/678/
                            </a>
                        </div>
                        <div class="search-result">
                            <div class="title-wrapper">
                                <a href="NewsPage?test" class="g-title-link">
                                    "Классный День" от ЦКО "МЕДИНВЕСТ"
                                </a>
                            </div>
                            <p class="description g-paragraph">
                                Школьники и их родные прошли профессиональный офтальмологический скрининг от ЦКО "МЕДИНВЕСТ".
                            </p>
                            <a href="http://oftalmolog36.ru/news/668/" class="link">
                                http://oftalmolog36.ru/news/668/
                            </a>
                        </div>
                        <div class="search-result">
                            <div class="title-wrapper">
                                <a href="NewsPage?test" class="g-title-link">
                                    "Мединвест" выходит на федеральный уровень
                                </a>
                            </div>
                            <p class="description g-paragraph">
                                18 марта состоялась конференция, на которой было подписано соглашение, согласно
                                которому Центр Клинической Офтальмологии "Мединвест" начнет работу на федеральном уровне.
                            </p>
                            <a href="http://oftalmolog36.ru/news/589/" class="link">
                                http://oftalmolog36.ru/news/589/
                            </a>
                        </div>
                        <div class="search-result">
                            <div class="title-wrapper">
                                <a href="NewsPage?test" class="g-title-link">
                                    Детский фестиваль "Кругозорчик"
                                </a>
                            </div>
                            <p class="description g-paragraph">
                                19 декабря 2015 года в Центре Галереи Чижова ЦКО "Мединвест" провел детский фестиваль "Кругозорчик".
                            </p>
                            <a href="http://oftalmolog36.ru/news/558/" class="link">
                                http://oftalmolog36.ru/news/558/
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/blocks/pagination.php' ?>
        </div>
    </div>
</div>
