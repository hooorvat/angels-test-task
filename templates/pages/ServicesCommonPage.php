<? require $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/header-white.php' ?>
<div class="p-content">
    <div class="container">
        <div class="b-services-common-page">
            <a name="diagnostic"></a>
            <div class="services-section">
                <div class="row">
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <div class="section-title">
                            <h2 class="title">
                                Диагностика
                            </h2>
                            <div class="description">
                                21 услуга
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-9 col-md-9 col-lg-9">
                        <ul class="services-container">
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    УЗИ глаза методом А-сканирования
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    УЗИ глаза методом В-сканирования
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Визометрия
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Компьютерная периметрия
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Консультация врача-офтальмолога
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Консультация детского офтальмолога
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Осмотр глазного дна с линзой Гольдмана
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Оптическая когеррентная томография ДЗН
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Оптическая когеррентная томография сетчатки
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Бесконтактная керратопахиметрия
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Бесконтактная биометрия
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Периметрия (ПНР)
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Авторефкератометрия
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Тонометрия по Маклакову
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Осмотр глазного дна с мидриазом
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Гониоскопия
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Бесконтактная пневмотонометрия
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Комплексное обследование при подозрении на глаукому
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Повторный осмотр для детей с обследованием с мидриазом
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Повторный осмотр с аппаратным обследованием для ребенка
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <a name="cataract-cure"></a>
            <div class="services-section">
                <div class="row">
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <div class="section-title">
                                <h2 class="title">
                                    Лечение катаракты
                                </h2>
                            <div class="description">
                                16 услуг
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-9 col-md-9 col-lg-9">
                        <ul class="services-container">
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Факоэмульсификация катаркты 1 категории сложности без имплантации ИОЛ
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Факоэмульсификация катаркты 2 категории сложности без имплантации ИОЛ
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Факоэмульсификация катаркты 1 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Факоэмульсификация катаркты 2 категории сложности с имплантацией монофокальной ИОЛ Bausch+Lomb Envista
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Факоэмульсификация катаркты 1 категории сложности с имплантацией монофокальной ИОЛ Alcon Acrysof IQ
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Факоэмульсификация катаркты 2 категории сложности с имплантацией монофокальной ИОЛ Alcon Acrysof IQ
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Факоэмульсификация катаркты 1 категории сложности с имплантацией монофокальной ИОЛ Abbott Tecnis
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Факоэмульсификация катаркты 2 категории сложности с имплантацией монофокальной ИОЛ Abbott Tecnis
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Факоэмульсификация катаркты (1 категории) с имплантацией торической ИОЛ Alcon Acrysof IQ Toric
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Факоэмульсификация катаркты (2 категории) с имплантацией торической ИОЛ Alcon Acrysof IQ Toric
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Факоэмульсификация катаркты с имплантацией мультифокальной ИОЛ Alcon Acrysof Restor
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Факоэмульсификация катаркты с имплантацией мультифокальной торической ИОЛ Alcon Acrysof Restor Toric
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Факоэмульсификация катаркты (1 категории) с имплантацией монофокальной ИОЛ HumanOptics Aspira-aAY
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Факоэмульсификация катаркты (2 категории) с имплантацией монофокальной ИОЛ HumanOptics Aspira-aA
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Факоэмульсификация катаркты (1 категории) с имплантацией монофокальной ИОЛ Zess CT ASPHINA 409M
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Факоэмульсификация катаркты (2 категории) с имплантацией монофокальной ИОЛ Zess CT ASPHINA 409M
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <a name="retina-cure"></a>
            <div class="services-section">
                <div class="row">
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <div class="section-title">
                            <h2 class="title">
                                Лазеркоагуляция сетчатки
                            </h2>
                            <div class="description">
                                11 услуг
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-9 col-md-9 col-lg-9">
                        <ul class="services-container">
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Дополнительная лазеркоагуляция сетчатки (после эндо-лазерной лазеркоагуляцией во время витрэктомии)
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Лазерная десцеметогониопунктура
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Лазерная десцеметогониопунктура (второй этап после НГСЭ в ООО "ЦКО "МЕДИВЕСТ")
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Лазерная операция ОЛК (ограничительная)
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Лазерная операция ППЛК (Периферическая профилактическая лазеркоагуляция сетчатки)
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Лазерная операция ПРЛК (панретинальная лазеркоагуляция сетчатки) - (стоимость одного сеанса)
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Лазерная операция СЛК (секторальная лазеркоагуляция)
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Лазерная операция ЛДГП
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Лазерная операция ЛИЭ (лазерная иридэктомия)
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Лазерная операция ЗЛКТ (задняя лазерная капсулотомия)
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Лазерная операция СЛТ (селективная лазерная трабекулопластика)
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <a name="glaucoma-cure"></a>
            <div class="services-section">
                <div class="row">
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <div class="section-title">
                            <h2 class="title">
                                Лечение глаукомы
                            </h2>
                            <div class="description">
                                5 услуг
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-9 col-md-9 col-lg-9">
                        <ul class="services-container">
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Непроникающая глубокая склерэктомия (НГСЭ)
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Синустрабекулоэктомия с задней склерэктомией (СТЗ+ЗСЭ)
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Непроникающая глубокая склерэктомия с имплантацией дренажа Glautex (НГСЭ+дренаж)
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Имплантация клапана Ahmed
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Имплантация шунта Express
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <a name="restoring-cure"></a>
            <div class="services-section">
                <div class="row">
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <div class="section-title">
                            <h2 class="title">
                                Восстановительное лечение глаз
                            </h2>
                            <div class="description">
                                22 услуги
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-9 col-md-9 col-lg-9">
                        <ul class="services-container">
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Оптические тренировки с линзами
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Оптические тренировки по Аветисову-Мац
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Оптические тренировки аккомодации вблизи ("Форбис")
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Светомагнитотерапия ("Амо-Атос")
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Цветоимпульсная терапия ("Цветоритм")
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Световая тренировка аккомодации ("Каскад")
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Тренировка аккомодации вблизи ("Ручеек")
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Электростимуляция ("Эсом-Комет")
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Электропунктура ("МнДэп")
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Лазерстимуляция ("Макдел")
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Лазерплеоптика ("Рубин")
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Компьютерная плеоптика
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Компьютерная ортоптика
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Компьютерная диплоптика
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Диплоптическое лечение ("Форбис")
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Лечение на синоптофоре
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Лечение на синоптофоре
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Лечебно-тренировочная программа
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Лечение "Визотроник"
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Видеокомпьютерная коррекция зрения ("Амблиокор")
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Магнитофорез
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Макулостимуляция
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Засветы по Кащенко
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <a name="other-services-and-operations"></a>
            <div class="services-section">
                <div class="row">
                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                        <div class="section-title">
                            <h2 class="title">
                                Прочие операции
                            </h2>
                            <div class="description">
                                11 услуг
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-9 col-md-9 col-lg-9">
                        <ul class="services-container">
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Удаление инородного тела роговицы
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Новообразования
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Птеригиум
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Халязион
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Удаление инородного тела
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Иридопластика
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Киста коньюктивы
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Ревизия поверхностного роговичного лоскута
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Пластика радужки
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Удаление кисты
                                </a>
                            </li>
                            <li class="service-wrapper">
                                <a href="ServicePage?test" class="service">
                                    Реконструкция передней камеры
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>