<div class="container">

    <?

    $urlAddress = "http://{$_SERVER['HTTP_HOST']}/";
    $templatesDirectory = $_SERVER['DOCUMENT_ROOT'] . $php_prefix . '/templates/pages';

    $templateFiles = array_filter(scandir($templatesDirectory), function($fileName) {
        // Exclude this file (e.g. 'index.php')
        return !in_array($fileName, array(basename(__FILE__)));
    });

    $templateFiles = array_filter($templateFiles, function($fileName) {
        return strpos($fileName, '.php');
    });


    $templateNames = array();

    foreach ($templateFiles as $templateFile) {
        $templateNames[] = str_replace('.php', '', $templateFile);
    }

    ?>

    <div class="bs-docs-section">

        <div class="row">
            <div class="col-lg-12">
                <div class="page-header">
                    <h1 id="tables">Ссылки на тестовые страницы</h1>
                </div>

                <div class="bs-component live-less-editor-hovercontainer"
                     data-relatedvars="state-success-bg,state-info-bg,state-warning-bg,state-danger-bg,table-bg,table-cell-padding,table-border-color,table-condensed-cell-padding,table-bg-accent,table-bg-hover,table-bg-active">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Ссылка</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? $index = 1;

                        foreach ($templateNames as $templateName) { ?>
                            <tr>
                                <td>
                                    <h4>
                                        <? echo $index; ?>
                                    </h4>
                                </td>
                                <td>
                                    <h4>
                                        <a target="_blank"
                                           href="<? echo $urlAddress . $php_prefix . ($isBack ? '?page=' . $templateName
                                                   : $templateName . $frontEndPostfix ); ?>">
                                            <? echo $templateName; ?>
                                        </a>
                                    </h4>
                                </td>
                            </tr>
                            <? $index++;

                        } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
