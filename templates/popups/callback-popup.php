<div class="callback-popup js-popup" id="callbackPopup">
    <div class="container">
        <div class="row">
            <div class="content">
                <span class="close js-close">
                    <svg height="20" width="20">
                        <line x1="0" y1="0" x2="20" y2="20" style="stroke:#000000; stroke-width:2"/>
                        <line x1="20" y1="0" x2="0" y2="20" style="stroke:#000000; stroke-width:2"/>
                    </svg>
                </span>
                <div class="inner-wrapper">
                    <div class="title col-xs-4 col-sm-12 col-md-12 col-lg-12">Заказать <br> обратный звонок</div>
                    <div class="col-xs-4 col-sm-hide col-md-hide col-lg-hide">
                        <div class="ask-question-form js-form">
                            <form class="f-form">
                                <div class="f-field">
                                    <span class="f-message"></span>
                                    <input class="f-input" type="text" name="name" required>
                                    <label class="f-label">Ваше имя</label>
                                </div>
                                <div class="f-field">
                                    <span class="f-message"></span>
                                    <input class="f-input js-phone-input" type="tel" name="phone" required>
                                    <label class="f-label">Телефон</label>
                                </div>
                                <div class="f-field">
                                    <input class="f-submit" type="submit" value="Заказать звонок">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-xs-hide col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4 desktop-container">
                        <div class="ask-question-form js-form">
                            <form class="f-form">
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="f-field">
                                        <span class="f-message"></span>
                                        <input class="f-input" type="text" name="name" required>
                                        <label class="f-label">Ваше имя</label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="f-field">
                                        <span class="f-message"></span>
                                        <input class="f-input js-phone-input" type="tel" name="phone" required>
                                        <label class="f-label">Телефон</label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="f-field">
                                        <input class="f-submit" type="submit" value="Заказать звонок">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>