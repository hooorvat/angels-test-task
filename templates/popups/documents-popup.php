<div class="b-documents-popup js-popup" id="documentsPopup">
    <div class="content">
        <div class="container">
            <div class="row">
                <span class="close js-close">
                    <svg height="20" width="20">
                        <line x1="0" y1="0" x2="20" y2="20" style="stroke:#fff; stroke-width:2"/>
                        <line x1="20" y1="0" x2="0" y2="20" style="stroke:#fff; stroke-width:2"/>
                    </svg>
                </span>
                <div class="col-xs-4 col-sm-12 col-md-12 col-lg-12">
                    <div class="b-slider js-slider">
                        <div class="row">
                                <div class="slider-viewport js-viewport">
                                    <div class="slides-container js-slides">
                                        <div class="slides-wrapper js-slides-wrapper">
                                            <div class="slide js-slide">
                                                <div class="document-image-wrapper">
                                                    <img class="document-image" src="../../site/src/images/images-fish/document-1.png">
                                                    <div class="substrate first"></div>
                                                    <div class="substrate second"></div>
                                                </div>
                                            </div>
                                            <div class="slide js-slide">
                                                <div class="document-image-wrapper">
                                                    <img class="document-image" src="../../site/src/images/images-fish/document-2.png">
                                                </div>
                                            </div>
                                            <div class="slide js-slide">
                                                <div class="document-image-wrapper">
                                                    <img class="document-image" src="../../site/src/images/images-fish/document-3.png">
                                                </div>
                                            </div>
                                            <div class="slide js-slide">
                                                <div class="document-image-wrapper">
                                                    <img class="document-image" src="../../site/src/images/images-fish/document-1.png">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="image-popup-previous-button js-left">
                                <svg class="svg-image" xmlns="http://www.w3.org/2000/svg" height="14.70711">
                                    <defs>
                                        <style>
                                            .svg-arrow {
                                                fill: none;
                                                stroke: #fff;
                                                stroke-miterlimit: 10;
                                            }
                                        </style>
                                    </defs>
                                    <title>arrow</title>
                                    <polyline class="svg-arrow" points="7.5 0.354 0.5 7.354 7.5 14.354"/>
                                </svg>
                            </div>
                            <div class="image-popup-next-button js-right">
                                <svg class="svg-image" xmlns="http://www.w3.org/2000/svg" height="14.70711">
                                    <defs>
                                        <style>
                                            .svg-arrow {
                                                fill: none;
                                                stroke: #fff;
                                                stroke-miterlimit: 10;
                                            }
                                        </style>
                                    </defs>
                                    <title>arrow</title>
                                    <polyline class="svg-arrow" points="7.5 0.354 0.5 7.354 7.5 14.354"/>
                                </svg>
                            </div>
                            <div class=" slider-controls col-sm-12 col-md-12 col-lg-12">
                                <ul class="slider-bulls-container js-bulls-container col-sm-offset-4 col-md-offset-4 col-lg-offset-4"></ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
