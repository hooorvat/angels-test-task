<div class="enroll-popup js-popup" id="enrollPopup">
    <div class="container">
        <div class="row">
            <div class="content">
                <span class="close js-close">
                    <svg height="20" width="20">
                        <line x1="0" y1="0" x2="20" y2="20" style="stroke:#000000; stroke-width:2"/>
                        <line x1="20" y1="0" x2="0" y2="20" style="stroke:#000000; stroke-width:2"/>
                    </svg>
                </span>
                <div class="inner-wrapper">
                    <div class="title col-xs-4 col-sm-12 col-md-12 col-lg-12">Записаться<br>на прием</div>
                    <div class="col-xs-4">
                        <div class="ask-question-form js-form">
                            <form class="f-form">
                                <div class="col-sm-3 col-sm-offset-3 col-md-3 col-md-offset-3 col-lg-3 col-lg-offset-3">
                                    <div class="f-field">
                                        <span class="f-message"></span>
                                        <input class="f-input" type="text" name="name" required>
                                        <label class="f-label">Ваше имя</label>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-md-3 col-lg-3">
                                    <div class="f-field">
                                        <span class="f-message"></span>
                                        <input class="f-input" type="text" name="surname" required>
                                        <label class="f-label">Ваша фамилия</label>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-sm-offset-3 col-md-3 col-md-offset-3 col-lg-3 col-lg-offset-3">
                                    <div class="f-field">
                                        <span class="f-message"></span>
                                        <input class="f-input" type="email" name="email" required>
                                        <label class="f-label">Email</label>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-md-3 col-lg-3">
                                    <div class="f-field">
                                        <span class="f-message"></span>
                                        <input class="f-input js-phone-input" type="tel" name="phone" required>
                                        <label class="f-label">Телефон</label>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-sm-offset-3 col-md-3 col-md-offset-3 col-lg-3 col-lg-offset-3">
                                    <div class="f-field">
                                        <div class="f-select-wrapper js-select">
                                            <select name="service" class="f-select">
                                                <option value="hide">
                                                    Тема приема
                                                </option>
                                                <option value="1">
                                                    Операции
                                                </option>
                                                <option value="2">
                                                    Общие вопросы
                                                </option>
                                                <option value="3">
                                                    Диагностика
                                                </option>
                                            </select>
                                            <div class="arrow"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-md-3 col-lg-3">
                                    <div class="f-field">
                                        <div class="f-select-wrapper js-select">
                                            <select name="service" class="f-select">
                                                <option value="hide">
                                                    Выберите врача
                                                </option>
                                                <option value="1">
                                                    Операции
                                                </option>
                                                <option value="2">
                                                    Общие вопросы
                                                </option>
                                                <option value="3">
                                                    Диагностика
                                                </option>
                                            </select>
                                            <div class="arrow"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">
                                    <div class="f-field">
                                        <span class="f-message"></span>
                                        <textarea class="f-textarea"></textarea>
                                        <label class="f-label">Сообщение</label>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">
                                    <div class="f-field">
                                        <input class="f-submit" type="submit" value="Отправить заявку">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>