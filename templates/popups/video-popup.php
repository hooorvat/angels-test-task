<div class="b-video-popup js-popup" id="videoPopup">
    <div class="video-wrapper js-video-player" data-video-type="youtube" data-video-id="LmfL0GW-QOM">
        <div class="exit-button js-close">
            <svg height="20" width="20">
                <line x1="0" y1="0" x2="20" y2="20" style="stroke:#FFFFFF; stroke-width:2"/>
                <line x1="20" y1="0" x2="0" y2="20" style="stroke:#FFFFFF; stroke-width:2"/>
            </svg>
        </div>
        <div class="inner js-inner"></div>
    </div>
</div>