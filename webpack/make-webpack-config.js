var webpack = require('webpack');
var path = require('path');
var fs = require('fs');
var _ = require('lodash');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var webpackConfig = {
    /* context: the base directory (absolute path!) for resolving the entry option.
     If output.pathinfo is set, the included pathinfo is shortened to this directory. */
    context: path.join(__dirname, '/../site/src'),
    entry: {
        main: './js/Application.es6',
        // wysiwyg: './less/wysiwyg.less',
    },
    output: {
        /* output.path: the output directory as absolute path (required). */
        path: path.join(__dirname, '/../site/assets'),
        /* output.filename: specifies the name of each output file on disk. You must not specify an absolute path here!
         The output.path option determines the location on disk the files are written to, filename is used solely for
         naming the individual files. [name] is replaced by the name of the chunk. [hash] is replaced by the hash of the
         compilation. [chunkhash] is replaced by the hash of the chunk. */
        filename: '[name].js',
        /* output.publicPath: the publicPath specifies the public URL address of the output files when referenced in a
         browser. For loaders that embed <script> or <link> tags or reference assets like images, publicPath is used as
         the href or url() to the file when it’s different then their location on disk (as specified by path). This can
         be helpful when you want to host some or all output files on a different domain or on a CDN. The Webpack Dev
         Server also takes a hint from publicPath using it to determine where to serve the output files from. As with
         path you can use the [hash] substitution for a better caching profile. */
        publicPath: '/site/assets/'
    },
    module: {

        loaders: [
            { test: /\.es6$/, exclude: /node_modules/, loader: 'babel', query: {presets: ['es2015']} },
            { test: /\.css$/, loader: ExtractTextPlugin.extract('style', 'css!autoprefixer') },
            { test: /\.less$/, loader: ExtractTextPlugin.extract('style', 'css!autoprefixer!less') },
            { test: /\.(jpe?g|png|gif|svg)$/i, loaders: ['url?limit=10000', 'img?minimize'] },
            { test: /\.(woff|woff2|eot|ttf)$/i, loader: 'url' },
            { test: /\.hbs$/i, loader: 'raw-loader' },
            { test: /\.json$/, loader: 'json-loader' },
            { test: /\.ya?ml/, loader: 'json-loader!yaml-loader' },
            { test: require.resolve('jquery'), loader: 'expose?$!expose?jQuery' },
            { test: require.resolve("lodash"), loader: "expose?_"  },
        ],
        preLoaders: []
    },
    resolve: {
        /* resolve.root: the directory (absolute path) that contains your modules. May also be an array of directories.
         This setting should be used to add individual directories to the search path. */
        root: [
            path.join(__dirname, '/../site/src'),
            path.join(__dirname, '/../site/src/js'),
            path.join(__dirname, '/../node_modules')
        ],
        /* resolve.alias: replace modules by other modules or paths. Expected is a object with keys being module names.
         The value is the new path. It’s similar to a replace but a bit more clever. If the the key ends with $ only the
         exact match (without the $) will be replaced. If the value is a relative path it will be relative to the file
         containing the require. */
        alias: {
            'inputmask.dependencyLib': path.join(__dirname, '/../node_modules/jquery.inputmask/extra/dependencyLibs/inputmask.dependencyLib.js'),
            'inputmask': path.join(__dirname, '/../node_modules/jquery.inputmask/dist/inputmask/inputmask.js')
        },
        fallback: __dirname
    },
    resolveLoader: {
        root: [
            path.join(__dirname, '/../node_modules'),
            path.join(__dirname, '/custom-loaders')
        ],
        alias: {
            'autoprefixer': 'autoprefixer-loader?browsers[]=last 2 version,browsers[]=IE 9'
        }
    },
    plugins: [
        new webpack.OldWatchingPlugin(),
        new webpack.optimize.CommonsChunkPlugin('common.js'),
        /* Automatically loaded modules. Module (value) is loaded when the identifier (key) is used as free variable in
         a module. The identifier is filled with the exports of the loaded module. */
        new webpack.ProvidePlugin({
            _: 'lodash',
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        }),
        /* DedupePlugin: search for equal or similar files and deduplicate them in the output. This comes with some
         overhead for the entry chunk, but can reduce file size effectively. This doesn’t change the module semantics at
         all. Don’t expect to solve problems with multiple module instance. They won’t be one instance after
         deduplication. Note: Don’t use it in watch mode. Only for production builds. */
        new webpack.optimize.DedupePlugin(),
        new ExtractTextPlugin('[name].css'),
        new webpack.optimize.UglifyJsPlugin({minimize: true})
    ],

    // Loaders configs
    imagemin: {
        gifsicle: {interlaced: false},
        jpegtran: {
            progressive: true,
            arithmetic: false
        },
        optipng: {optimizationLevel: 5},
        pngquant: {
            floyd: 0.5,
            speed: 2
        },
        svgo: {
            plugins: [
                {removeTitle: true},
                {convertPathData: false}
            ]
        }
    }
};

var isLintConfigExists = fs.existsSync(path.join(__dirname, './../.eslintrc'));

if (isLintConfigExists) {
    webpackConfig.module.preLoaders.push(
        {test: /\.es6$/, exclude: /node_modules/, loader: 'eslint-loader'}
    );

    webpackConfig.eslint = {configFile: '.eslintrc'};
}

module.exports = function(options) {
    var devServerUrl = 'http://localhost:8080';

    if (options.dev) {
        webpackConfig.output.publicPath = devServerUrl + '/site/assets/';
        webpackConfig.devtools = 'eval';

        _.find(webpackConfig.module.loaders, { test: /\.css$/ }).loader = 'style!css!autoprefixer';
        _.find(webpackConfig.module.loaders, { test: /\.less$/ }).loader = 'style!css!autoprefixer!less';
        _.find(webpackConfig.module.loaders, { test: /\.(jpe?g|png|gif|svg)$/i }).loader = 'url?limit=10000';

        _.remove(webpackConfig.plugins, new webpack.optimize.UglifyJsPlugin({minimize: true}));
        _.remove(webpackConfig.plugins, new ExtractTextPlugin('[name].css'));
    }

    if (options.test) {
        webpackConfig.output.path = path.join(__dirname, '/../test/site/assets');
        webpackConfig.output.publicPath = '../site/test/assets/';

        _.find(webpackConfig.module.loaders, { test: /\.(jpe?g|png|gif|svg)$/i }).loader = 'url?limit=10000';

        _.remove(webpackConfig.plugins, new webpack.optimize.UglifyJsPlugin({minimize: true}));
    }

    return webpackConfig;
};
